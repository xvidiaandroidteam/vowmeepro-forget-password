
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-keepattributes *Annotation*
-libraryjars <java.home>/lib/rt.jar(java/**,javax/**)

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-ignorewarnings

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keep public class * implements java.io.Serializable
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.support.v4.app.ListFragment
-keep public class android.content.** { *; }
-dontwarn java.lang.invoke**
-dontwarn org.apache.lang.**
-dontwarn org.apache.commons.**
-dontwarn com.android.volley.**
-dontwarn com.netcompss.ffmpeg4android.**
-keep class com.android.volley.** { *; }
-keep interface com.android.volley.** { *; }
-keep class net.xvidia.vowmee.enterprise.network.model.**{ *; }
-keepclassmembernames class net.xvidia.vowmee.enterprise.network.model.** { *; }
-keepclasseswithmembers class net.xvidia.vowmee.enterprise.network.model.** { *;}

-keep public class net.xvidia.vowmee.enterprise.network.model.** {
  public void set*(***);
  public *** get*();
}

-keepattributes *Annotation*,EnclosingMethod,Signature
-keep class com.fasterxml.jackson.databind.**{ *; }
-keep class com.fasterxml.jackson.core.**{ *; }
-keepclassmembers class * implements android.os.Parcelable {
    static android.os.Parcelable$Creator CREATOR;
}
-keep class com.fasterxml.jackson.annotations.**{ *; }
-keepnames class com.fasterxml.jackson.** { *; }
 -dontwarn com.fasterxml.jackson.databind.**
 -keep class org.codehaus.** { *; }
 -keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
 public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *;
 }
-keep public class io.vov.vitamio.MediaPlayer { *; }
-keep public class io.vov.vitamio.IMediaScannerService { *; }
-keep public class io.vov.vitamio.MediaScanner { *; }
-keep public class io.vov.vitamio.MediaScannerClient { *; }
-keep public class io.vov.vitamio.VitamioLicense { *; }
-keep public class io.vov.vitamio.MediaMetadataRetriever { *; }
-keep public class io.vov.vitamio.** { *; }
-keep public class io.vov.vitamio.Vitamio { *; }
-keep public class io.vov.vitamio.utils.** { *; }
-keep public class io.vov.vitamio.provider.** { *; }
-keep public class io.vov.vitamio.activity.** { *; }
-keep public class io.vov.vitamio.widget.** { *; }