package net.xvidia.vowmee.enterprise;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.xvidia.vowmee.enterprise.network.ModelManager;

import java.util.ArrayList;

public class CustomGrid extends BaseAdapter {
      private Context mContext;
      private final String[] web;
      private final int[] Imageid;
    private final int[] Colorid;
    private ArrayList<Integer> countArray;
    private static LayoutInflater inflater = null;
    int mSize;
 
        public CustomGrid(Context c, String[] web,int[] Imageid,int[] Colorid,  int size) {
            mSize = size;
            mContext = c;
            this.Imageid = Imageid;
            this.web = web;
            this.Colorid = Colorid;
            countArray = new ArrayList<>();
            countArray.add(0,ModelManager.getInstance().getAllHubInfoList().size());
            countArray.add(1,ModelManager.getInstance().getAllCircleInfoList().size());
            countArray.add(2,ModelManager.getInstance().getAllMicoInfoList().size());
//            countArray.add(3,ModelManager.getInstance().getAllClusterVoList().size());
            countArray.add(3, ModelManager.getInstance().getAllCameraVoList().size());
            countArray.add(4, ModelManager.getInstance().getAllCrsCameraVoList().size());
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
 
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return web.length;
        }
 
        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }
 
        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View grid = convertView;
            ViewHolder viewHolder;
            if (convertView == null) {
                grid = inflater.inflate(R.layout.grid_single, null);
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) grid.findViewById(R.id.grid_text);
                viewHolder.relativeLayout = (RelativeLayout) grid.findViewById(R.id.relative_layout);
                viewHolder.imageView = (ImageView) grid.findViewById(R.id.grid_image);
                viewHolder.count = (TextView) grid.findViewById(R.id.count);
                grid.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, mSize));
                grid.setTag(viewHolder);
            }else {
                viewHolder = (ViewHolder) grid.getTag();
            }
            viewHolder.count.setText(countArray.get(position).toString());
            viewHolder.imageView.setImageDrawable((mContext.getResources().getDrawable(Imageid[position])));
            viewHolder.textView.setText(web[position]);
            viewHolder.relativeLayout.setBackgroundColor(mContext.getResources().getColor(Colorid[position]));
            return grid;
        }



    public static class ViewHolder {
        public TextView textView;
        public RelativeLayout relativeLayout ;
        public ImageView imageView;
        public TextView count ;

    }


}
