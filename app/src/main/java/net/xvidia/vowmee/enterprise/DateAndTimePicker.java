package net.xvidia.vowmee.enterprise;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.Util.MyExceptionHandler;
import net.xvidia.vowmee.enterprise.storage.sqlite.DataStorage;
import net.xvidia.vowmee.enterprise.network.ServiceURLManager;
import net.xvidia.vowmee.enterprise.network.VolleySingleton;
import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.network.model.Login;
import io.vov.vitamio.Vitamio;
import me.tittojose.www.timerangepicker_library.TimeRangePickerDialog;

/**
 * Created by vasu on 28/3/16.
 */
public class DateAndTimePicker extends AppCompatActivity implements TimeRangePickerDialog.OnTimeRangeSelectedListener {

    private Toolbar toolbar;
    //    CalendarView calendarView;
    private View mProgressView;
    TextView tvRecorded;
    public static final String TIMERANGEPICKER_TAG = "timerangepicker";

    private static CameraVoList cameraObj;
    private int mYear, mMonth, mDay;
    private String date;
    //    boolean timePickerFlag;
    //    DatePicker datePicker;
    private static boolean trigger = true;

    private String liveUrl, cameraStatusUrl;

    public static CameraVoList getCameraVoListObj() {
        return cameraObj;
    }

    public static void setCameraVoListObj(CameraVoList cameraObj) {
        DateAndTimePicker.cameraObj = cameraObj;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time_picker);
        Vitamio.isInitialized(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgressView = findViewById(R.id.login_progress);

//        String initialStatus = cameraObj.getCameraStatus();
//        calendarView = (CalendarView) findViewById(R.id.calender);
//        datePicker = (DatePicker) findViewById(R.id.datepicker);

//        tvLive = (TextView) findViewById(R.id.liveVideos);
//        tvLive.setText("Camera "+ initialStatus);
//        tvLive.setClickable(false);
        tvRecorded = (TextView) findViewById(R.id.recordedVideos);

        if (cameraObj == null)
            finish();
//        if (cameraObj.getStrLiveUrl().isEmpty()) {
//            tvLive.setVisibility(View.GONE);
//            calendarView.setVisibility(View.VISIBLE);
//            datePicker.setVisibility(View.VISIBLE);
            showDialog(999);
//        } else {

//            tvLive.setVisibility(View.VISIBLE);
//            calendarView.setVisibility(View.VISIBLE);
//            datePicker.setVisibility(View.VISIBLE);
//        }
//        long id = cameraObj.getId();
//        cameraStatusUrl = ServiceURLManager.getInstance().getCameraStatusUrl(id);
//        checkCameraCurrentStatus(cameraStatusUrl);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(cameraObj.getCameraName());
        getSupportActionBar().setSubtitle(cameraObj.getClusterName());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, Login.class));

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();

    }
    /* private void checkCameraCurrentStatus(String url) {

        try {

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url,"{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        String message = response.getString("message");
                        int statusCode = response.getInt("statusCode");
                        if(statusCode == 200) {
                            if(message.equalsIgnoreCase("OFF")){
                                showDialog(999);
//                                tvLive.setClickable(false);
                            }else{
//                                tvLive.setClickable(true);
                            }

//                            tvLive.setText("Camera " + message);

                            if(message.equalsIgnoreCase("ON")){

                                long id = cameraObj.getId();
                                String username = DataStorage.getInstance().getUsername();
                                liveUrl = ServiceURLManager.getInstance().getStartVideoUrl(id, username);

                                sendStartVideoRequest(liveUrl);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error != null && error.networkResponse == null){
                        Log.e("error",error);
                        showError(getString(R.string.error_general), null);
                        finish();
                    }
                }

            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }


    }*/

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub

        if (id == 999) {
            Calendar calendar = Calendar.getInstance();
            Date currentDate = calendar.getTime();

            DatePickerDialog dialog = new DatePickerDialog(this, R.style.AppCompatAlertDialogStyle, myDateListener, new Date().getYear(), new Date().getMonth(), new Date().getDate());
            dialog.getDatePicker().updateDate(new Date().getYear(), new Date().getMonth(), new Date().getDate());
            dialog.getDatePicker().setMaxDate(currentDate.getTime());

            calendar.add(Calendar.DATE, -14);
            Date minDate = calendar.getTime();
            dialog.getDatePicker().setMinDate(minDate.getTime());
            return dialog;
        }

        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // TODO Auto-generated method stub

            if (view.isShown()) {
                mYear = year;
                mMonth = month + 1;
                mDay = day;
                String monthZero = "0";
                String dayZero = "0";
                if (mMonth < 10)
                    monthZero = monthZero + mMonth;
                else
                    monthZero = "" + mMonth;

                if (mDay < 10)
                    dayZero = dayZero + mDay;
                else
                    dayZero = "" + mDay;
                date = "" + mYear + "-" + monthZero + "-" + dayZero;
                final TimeRangePickerDialog timePickerDialog = TimeRangePickerDialog.newInstance(
                        DateAndTimePicker.this, false);
                timePickerDialog.show(getSupportFragmentManager(), TIMERANGEPICKER_TAG);
//                    trigger = false;
//                    return;
            }
        }
//                trigger = true;
//            }
    };

//    public void callLiveVideos(View v) {
//
//        long id = cameraObj.getId();
//        String username = DataStorage.getInstance().getUsername();
//
//        liveUrl = ServiceURLManager.getInstance().getStartVideoUrl(id, username);
//
//        sendStartVideoRequest(liveUrl);
//
//    }

//    private void sendStartVideoRequest(String url) {
//
//        try {
//            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {
//
//                @Override
//                public void onResponse(JSONObject response) {
//                    ObjectMapper mapper = new ObjectMapper();
//                    CameraStatus obj = null;
//                    try {
//                        obj = mapper.readValue(response.toString(), CameraStatus.class);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    if(obj !=null) {
//                        int statusCode = obj.getStatusCode();
//                        String message = obj.getMessage();
//                        if(statusCode == 200) {
//                            VideoViewActivity.setLiveUrl(cameraObj.getStrLiveUrl());
//                            Intent intent = new Intent(DateAndTimePicker.this, VideoViewActivity.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putBoolean(AppConstant.DIRECT, true);
//                            intent.putExtras(bundle);
//                            startActivity(intent);
//                        }else{
//                            showError(message,null);
//                        }
//                    }
//                }
//            }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//
//                    if (error != null && error.networkResponse == null){
//                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
//                            }
//                        });
//                    }
//                }
//
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<String, String>();
////                    params.put("username", DataStorage.getInstance().getUsername());
////                    params.put("password",DataStorage.getInstance().getPassword());
//                    params.put("Authorization", getAuthHeader());
//                    return params;
//                }
//            };
//            request.setRetryPolicy(new DefaultRetryPolicy(
//                    30000,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
////            final JsonObjectRequest requestFinal = request;
//            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
////			}
//        } catch (Exception e) {
//
//        }
//
//    }

    public void callRecordedVideos(View v) {
//        calendarView.setVisibility(View.VISIBLE);
//        datePicker.setVisibility(View.VISIBLE);
        showDialog(999);

    }

    @Override
    public void onTimeRangeSelected(int startHour, int startMin, int endHour, int endMin) {

        String strHr = "" + startHour;
        String strMin = "" + startMin;
        String endHr = "" + endHour;
        String endMn = "" + endMin;
        if (strHr.length() == 1)
            strHr = "0" + strHr;
        if (strMin.length() == 1)
            strMin = "0" + strMin;
        if (endHr.length() == 1)
            endHr = "0" + endHr;
        if (endMn.length() == 1)
            endMn = "0" + endMn;

        String time = strHr + ":" + strMin + "/" + endHr + ":" + endMn;
//        String url = "http://192.168.10.111:9090/getPlayBackFilelist/"+cameraObj.getCamera().getId()+"/"+date+"/"+time;

        String url = ServiceURLManager.getInstance().getCameraFileListUrl("" + cameraObj.getId(), date, time);

        getPlayList(url);
    }

//    private String getAuthHeader() {
//
//        String authHeader = "";
//
//        try {
//            String auth = DataStorage.getInstance().getUsername() + ":" + DataStorage.getInstance().getPassword();
////            String auth = "username:username";
//            byte[] data = auth.getBytes("UTF-8");
//            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
//            authHeader = "Basic " + new String(base64);
//
//        } catch (UnsupportedEncodingException e) {
//
//        }
////
//        return authHeader;
//    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation rotation = AnimationUtils.loadAnimation(DateAndTimePicker.this, R.anim.rotate);
                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getPlayList(String url) {
        try {
            showProgress(true);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    ObjectMapper mapper = new ObjectMapper();
                    List<String> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<String>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            VideoViewActivity.setCameraFileList((ArrayList) obj);
                            Intent intent = new Intent(DateAndTimePicker.this, VideoViewActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(AppConstant.DIRECT, false);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else {

                            showError(getString(R.string.error_general), null);
                        }
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                    showProgress(false);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    showProgress(false);
                    showError(getString(R.string.error_general), null);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }
    private String getAuthHeader() {

        String authHeader = "";

        try {
            String auth = DataStorage.getInstance().getUsername() + ":" + DataStorage.getInstance().getPassword();
//            String auth = "username:username";
            byte[] data = auth.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            authHeader = "Basic " + new String(base64);

        } catch (UnsupportedEncodingException e) {

        }
//
        return authHeader;
    }
}
