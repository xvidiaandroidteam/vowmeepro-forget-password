package net.xvidia.vowmee.enterprise;

/**
 * Created by vasu on 28/3/16.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.Util.MyExceptionHandler;
import net.xvidia.vowmee.enterprise.listadapter.CameraListAdapter;
import net.xvidia.vowmee.enterprise.listadapter.CircleListAdapter;
import net.xvidia.vowmee.enterprise.listadapter.ClusterListAdapter;
import net.xvidia.vowmee.enterprise.listadapter.HubListAdapter;
import net.xvidia.vowmee.enterprise.listadapter.MicoListAdapter;
import net.xvidia.vowmee.enterprise.network.ModelManager;
import net.xvidia.vowmee.enterprise.network.ServiceURLManager;
import net.xvidia.vowmee.enterprise.network.VolleySingleton;
import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.network.model.CircleInfoList;
import net.xvidia.vowmee.enterprise.network.model.ClusterVOList;
import net.xvidia.vowmee.enterprise.network.model.HubInfoList;
import net.xvidia.vowmee.enterprise.network.model.Login;
import net.xvidia.vowmee.enterprise.network.model.MicoInfoList;
import net.xvidia.vowmee.enterprise.storage.sqlite.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.vov.vitamio.utils.Log;

public class ListActivity extends AppCompatActivity {
    ArrayAdapter<String> adapter;
    ArrayList<String> items;
    private EditText mSearchQueryTextView;
    public static RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<HubInfoList> cameraList;
    String passedIntentValue;
    boolean directList;
    private TextView noData;
    private static boolean crsFlag;
    public static View mProgressView;
    public static Map<String, CameraVoList> map;
    private boolean backpressedClosed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_list_activity);
        Toolbar toolbar;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Camera");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        noData = (TextView) findViewById(R.id.noData);
//		getSupportActionBar().setIcon(R.drawable.idea_logo);
        mRecyclerView = (RecyclerView) findViewById(R.id.listv);
        mRecyclerView.setHasFixedSize(true);
        mSearchQueryTextView = (EditText) findViewById(R.id.search_query);
        mProgressView = findViewById(R.id.list_progress);
        mProgressView.setVisibility(View.GONE);
        mSearchQueryTextView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                cs = cs.toString().toLowerCase();

                if (directList) {
                    if (passedIntentValue.contentEquals(AppConstant.HUB)) {

                        final ArrayList<HubInfoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllHubInfoList().size(); i++) {
                            final HubInfoList hubInfoList = ModelManager.getInstance().getAllHubInfoList().get(i);
                            final String text = hubInfoList.getHubName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllHubInfoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new HubListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.CIRCLE)) {

                        final ArrayList<CircleInfoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllCircleInfoList().size(); i++) {
                            final CircleInfoList circleInfoList = ModelManager.getInstance().getAllCircleInfoList().get(i);
                            final String text = circleInfoList.getCircleName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllCircleInfoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new CircleListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.MICO)) {

                        final ArrayList<MicoInfoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllMicoInfoList().size(); i++) {
                            final MicoInfoList micoInfoList = ModelManager.getInstance().getAllMicoInfoList().get(i);
                            final String text = micoInfoList.getMicoName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllMicoInfoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new MicoListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.CLUSTER)) {

                        final ArrayList<ClusterVOList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllClusterVoList().size(); i++) {
                            final ClusterVOList clusterVOList = ModelManager.getInstance().getAllClusterVoList().get(i);
                            final String text = clusterVOList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllClusterVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new ClusterListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.CAMERA)) {

                        final ArrayList<CameraVoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllCameraVoList().size(); i++) {
                            final CameraVoList cameraVoList = ModelManager.getInstance().getAllCameraVoList().get(i);
                            final String text = cameraVoList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllCameraVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new CameraListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        /*final ArrayList<ClusterVOList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllClusterVoList().size(); i++) {
                            final ClusterVOList clusterVOList = ModelManager.getInstance().getAllClusterVoList().get(i);
                            final String text = clusterVOList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllClusterVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new ClusterListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();*/

                    } else if (passedIntentValue.contentEquals(AppConstant.CRS_CAMERA)) {

                        final ArrayList<CameraVoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getAllCrsCameraVoList().size(); i++) {
                            final CameraVoList cameraVoList = ModelManager.getInstance().getAllCrsCameraVoList().get(i);
                            final String text = cameraVoList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getAllCrsCameraVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new CameraListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    }
                } else {

                    if (passedIntentValue.contentEquals(AppConstant.HUB)) {

                        final ArrayList<HubInfoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getHubInfoLists().size(); i++) {
                            final HubInfoList hubInfoList = ModelManager.getInstance().getHubInfoLists().get(i);
                            final String text = hubInfoList.getHubName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getHubInfoLists().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new HubListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.CIRCLE)) {

                        final ArrayList<CircleInfoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getCircleInfoList().size(); i++) {
                            final CircleInfoList circleInfoList = ModelManager.getInstance().getCircleInfoList().get(i);
                            final String text = circleInfoList.getCircleName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getCircleInfoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new CircleListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.MICO)) {

                        final ArrayList<MicoInfoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getMicoInfoList().size(); i++) {
                            final MicoInfoList micoInfoList = ModelManager.getInstance().getMicoInfoList().get(i);
                            final String text = micoInfoList.getMicoName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getMicoInfoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new MicoListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.CLUSTER)) {

                        final ArrayList<ClusterVOList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getClusterVoList().size(); i++) {
                            final ClusterVOList clusterVOList = ModelManager.getInstance().getClusterVoList().get(i);
                            final String text = clusterVOList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getClusterVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new ClusterListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else if (passedIntentValue.contentEquals(AppConstant.CAMERA)) {

                        final ArrayList<CameraVoList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getCameraVoList().size(); i++) {
                            final CameraVoList cameraVoList = ModelManager.getInstance().getCameraVoList().get(i);
                            final String text = cameraVoList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getCameraVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new CameraListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                       /* final ArrayList<ClusterVOList> filteredList = new ArrayList<>();
                        for (int i = 0; i < ModelManager.getInstance().getClusterVoList().size(); i++) {
                            final ClusterVOList clusterVOList = ModelManager.getInstance().getClusterVoList().get(i);
                            final String text = clusterVOList.getClusterName().toString().toLowerCase();
                            if (text.contains(cs)) {
                                filteredList.add(ModelManager.getInstance().getClusterVoList().get(i));
                            }
                        }
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ListActivity.this));
                        mAdapter = new ClusterListAdapter(ListActivity.this, filteredList);
                        mRecyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();*/

                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        Intent intent = getIntent();
        if (null != intent) {
            passedIntentValue = intent.getStringExtra(AppConstant.NEXTLIST);
            directList = intent.getBooleanExtra(AppConstant.DIRECT, true);
            backpressedClosed = intent.getBooleanExtra(AppConstant.BACKPRESSED_CLOSED, false);
        }
        if (backpressedClosed) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        if (directList) {
            if (passedIntentValue.contentEquals(AppConstant.HUB)) {
                mSearchQueryTextView.setHint("Search Circle");
                mAdapter = new HubListAdapter(this, ModelManager.getInstance().getAllHubInfoList());
                initialiseRecyclerView(mAdapter);
            } else if (passedIntentValue.contentEquals(AppConstant.CIRCLE)) {
                mSearchQueryTextView.setHint("Search Zone");
                mAdapter = new CircleListAdapter(this, ModelManager.getInstance().getAllCircleInfoList());
                initialiseRecyclerView(mAdapter);
            } else if (passedIntentValue.contentEquals(AppConstant.MICO)) {
                mSearchQueryTextView.setHint("Search Mico");
                mAdapter = new MicoListAdapter(this, ModelManager.getInstance().getAllMicoInfoList());
                initialiseRecyclerView(mAdapter);
            } else if (passedIntentValue.contentEquals(AppConstant.CLUSTER)) {
                mSearchQueryTextView.setHint("Search Store");
                mAdapter = new ClusterListAdapter(this, ModelManager.getInstance().getAllClusterVoList());
                initialiseRecyclerView(mAdapter);
            } else if (passedIntentValue.contentEquals(AppConstant.CAMERA)) {
                mSearchQueryTextView.setHint("Search Store");
                mAdapter = new CameraListAdapter(this, ModelManager.getInstance().getAllCameraVoList());
                initialiseRecyclerView(mAdapter);
                crsFlag = false;
                String allCameraStatusUrl = ServiceURLManager.getInstance().getAllCameraStatusUrl();
                checkAllCameraCurrentStatus(allCameraStatusUrl, false,true);
//                mAdapter = new CameraListAdapter(this, ModelManager.getInstance().getAllCameraVoList());
            } else if (passedIntentValue.contentEquals(AppConstant.CRS_CAMERA)) {
                mSearchQueryTextView.setHint("Search CRS");
                mAdapter = new CameraListAdapter(this, ModelManager.getInstance().getAllCrsCameraVoList());
                initialiseRecyclerView(mAdapter);
                crsFlag = true;
                String allCameraStatusUrl = ServiceURLManager.getInstance().getAllCameraStatusUrl();
                checkAllCameraCurrentStatus(allCameraStatusUrl, true,true);
            }

        } else {
            if (passedIntentValue.contentEquals(AppConstant.HUB)) {
                mSearchQueryTextView.setHint("Search Circle");
                if (ModelManager.getInstance().getHubInfoLists().size() < 1 || ModelManager.getInstance().getHubInfoLists() == null) {
                    noData.setVisibility(View.VISIBLE);
                    noData.setText("No Circle found");
                } else {
                    mAdapter = new HubListAdapter(this, ModelManager.getInstance().getHubInfoLists());
                    initialiseRecyclerView(mAdapter);
                }
            } else if (passedIntentValue.contentEquals(AppConstant.CIRCLE)) {
                mSearchQueryTextView.setHint("Search Zone");
                if (ModelManager.getInstance().getCircleInfoList().size() < 1 || ModelManager.getInstance().getCircleInfoList() == null) {
                    noData.setVisibility(View.VISIBLE);
                    noData.setText("No Zone found");
                } else {
                    mAdapter = new CircleListAdapter(this, ModelManager.getInstance().getCircleInfoList());
                    initialiseRecyclerView(mAdapter);
                }
            } else if (passedIntentValue.contentEquals(AppConstant.MICO)) {
                mSearchQueryTextView.setHint("Search Mico");
                if (ModelManager.getInstance().getMicoInfoList().size() < 1 || ModelManager.getInstance().getMicoInfoList() == null) {
                    noData.setVisibility(View.VISIBLE);
                    noData.setText("No Mico found");
                } else {
                    mAdapter = new MicoListAdapter(this, ModelManager.getInstance().getMicoInfoList());
                    initialiseRecyclerView(mAdapter);
                }
            } else if (passedIntentValue.contentEquals(AppConstant.CLUSTER)) {
                mSearchQueryTextView.setHint("Search Cluster");
                if (ModelManager.getInstance().getClusterVoList().size() < 1 || ModelManager.getInstance().getClusterVoList() == null) {
                    noData.setVisibility(View.VISIBLE);
                    noData.setText("No Store found");
                } else {
                    mAdapter = new ClusterListAdapter(this, ModelManager.getInstance().getClusterVoList());
                    initialiseRecyclerView(mAdapter);
                }
            } else if (passedIntentValue.contentEquals(AppConstant.CAMERA)) {
                mSearchQueryTextView.setHint("Search Store");
                crsFlag = false;
                if (ModelManager.getInstance().getCameraVoList().size() < 1 || ModelManager.getInstance().getCameraVoList() == null) {
                    noData.setVisibility(View.VISIBLE);
                    noData.setText("No Camera found");
                } else {
                    String allCameraStatusUrl = ServiceURLManager.getInstance().getAllCameraStatusUrl();
                    checkAllCameraCurrentStatus(allCameraStatusUrl, false,true);
                    mAdapter = new CameraListAdapter(this, ModelManager.getInstance().getCameraVoList());
                    initialiseRecyclerView(mAdapter);
                }
            }

        }

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, MainActivity.class));


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (backpressedClosed) {
            finish();
        }
    }

    private void checkAllCameraCurrentStatus(String url, final boolean flag, final boolean updateView) {

        showProgress(false);
        try {

//            Login login = new Login(DataStorage.getInstance().getUsername(), DataStorage.getInstance().getPassword());
            Login login = new Login();
            login.setUsername(DataStorage.getInstance().getUsername());
            login.setPassword(DataStorage.getInstance().getPassword());
            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;

            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

//                            Log.e("response", response);
                            try {

                                ObjectMapper mapper = new ObjectMapper();

                                map = new HashMap<String, CameraVoList>();

                                map = mapper.readValue(response.toString(), new TypeReference<Map<String, CameraVoList>>() {
                                });

                            } catch (JsonGenerationException e) {
                                e.printStackTrace();
                            } catch (JsonMappingException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            if (updateView) {
                                if (directList) {
                                    if (passedIntentValue.contentEquals(AppConstant.CAMERA) && !crsFlag && !flag) {
                                        mAdapter = new CameraListAdapter(ListActivity.this, ModelManager.getInstance().getAllCameraVoList());
                                        initialiseRecyclerView(mAdapter);
                                    } else if (crsFlag && flag) {
                                        mAdapter = new CameraListAdapter(ListActivity.this, ModelManager.getInstance().getAllCrsCameraVoList());
                                        initialiseRecyclerView(mAdapter);
                                    }
                                } else {
                                    mAdapter = new CameraListAdapter(ListActivity.this, ModelManager.getInstance().getCameraVoList());
                                    initialiseRecyclerView(mAdapter);
                                }

                            }
                            showProgress(false);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("response", error);
                    showProgress(false);
                    if (error != null && error.networkResponse == null) {
                        Log.e("error", error);
                        showError(getString(R.string.error_general), null);
//                        finish();

                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }

    }

    public void showProgress(final boolean show) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation rotation = AnimationUtils.loadAnimation(ListActivity.this, R.anim.rotate);
                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(ListActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }


    private void initialiseRecyclerView(RecyclerView.Adapter mAdapter) {

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();


    }

    //    public void onStart(){
//        super.onStart();
//        // Create request queue
//        RequestQueue requestQueue= Volley.newRequestQueue(this);
//        //  Create json array request
//        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest("",new Response.Listener<JSONArray>(){
//            public void onResponse(JSONArray jsonArray){
//                // Successfully download json
//                // So parse it and populate the listview
//                for(int i=0;i<jsonArray.length();i++){
//                    try {
//                        JSONObject jsonObject=jsonArray.getJSONObject(i);
//                        items.add(jsonObject.getString("cameraname"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                adapter.notifyDataSetChanged();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.e("Error", "Unable to parse json array");
//            }
//        });
//        // add json array request to the request queue
//        requestQueue.add(jsonArrayRequest);
//    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem logout = menu.findItem(R.id.action_logout);
        MenuItem home = menu.findItem(R.id.action_home);
        if (backpressedClosed) {
            logout.setVisible(true);
            home.setVisible(false);
        } else {
            logout.setVisible(false);
            home.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_home) {
            Intent intent = new Intent(ListActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        if (id == R.id.action_logout) {

            new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Logout")
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//							getSharedPreferences(DataStorage.MY_PREFERENCES,MODE_PRIVATE).edit().clear().commit();
                            DataStorage.getInstance().removeDataStorage();
                            Intent intent = new Intent(ListActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRecyclerView != null) {
            mRecyclerView.setClickable(true);
        }
        if (passedIntentValue != null) {
            if (passedIntentValue.contentEquals(AppConstant.CRS_CAMERA) || passedIntentValue.contentEquals(AppConstant.CAMERA)) {
                String allCameraStatusUrl = ServiceURLManager.getInstance().getAllCameraStatusUrl();
                checkAllCameraCurrentStatus(allCameraStatusUrl, crsFlag, false);
            }
        }
    }
}
