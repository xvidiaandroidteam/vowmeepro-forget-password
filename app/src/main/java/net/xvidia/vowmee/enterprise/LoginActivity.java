package net.xvidia.vowmee.enterprise;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.enterprise.Util.MyExceptionHandler;
import net.xvidia.vowmee.enterprise.network.ModelManager;
import net.xvidia.vowmee.enterprise.network.ServiceURLManager;
import net.xvidia.vowmee.enterprise.network.VolleySingleton;
import net.xvidia.vowmee.enterprise.network.model.HubInfoList;
import net.xvidia.vowmee.enterprise.network.model.Login;
import net.xvidia.vowmee.enterprise.storage.sqlite.DataStorage;
import net.xvidia.vowmee.enterprise.storage.sqlite.manager.DataCacheManager;

import org.json.JSONArray;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private TextView mShowPassword, forgotPassword;
    private CheckBox mCheckBoxRemeber;
    private static boolean disableFlag;
    private Button mEmailSignInButton;
    private boolean remeberMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.

//        SharedPreferences preferences = getSharedPreferences(DataStorage.STR_RESPONSE, 0);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.remove(DataStorage.STR_RESPONSE);
//        editor.clear();
//        editor.commit();
//        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.login));
        mEmailView = (EditText) findViewById(R.id.email);
        mShowPassword = (TextView) findViewById(R.id.login_showpassword);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
        forgotPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        mPasswordView = (EditText) findViewById(R.id.password);
        mCheckBoxRemeber = (CheckBox) findViewById(R.id.checkBox_remember);


        mCheckBoxRemeber.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                remeberMe = isChecked ;
            }
        });
        remeberMe = DataStorage.getInstance().getRemember();
        mCheckBoxRemeber.setChecked(remeberMe);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    disableFlag = false;
                    attemptLogin();

            }
        });

        mShowPassword.setVisibility(View.GONE);
        mShowPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final boolean isOutsideView = event.getX() < 0 ||
                        event.getX() > v.getWidth() ||
                        event.getY() < 0 ||
                        event.getY() > v.getHeight();

                // change input type will reset cursor position, so we want to save it
                final int cursor = mPasswordView.getSelectionStart();

                if (isOutsideView || MotionEvent.ACTION_UP == event.getAction())
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                else
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

                mPasswordView.setSelection(cursor);
                return true;
            }
        });

        mPasswordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mShowPassword.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });
        mProgressView = findViewById(R.id.login_progress);
        if(DataStorage.getInstance().getRemember() &&!DataStorage.getInstance().getUsername().isEmpty()
                &&!DataStorage.getInstance().getPassword().isEmpty()){
            mEmailView.setText(DataStorage.getInstance().getUsername());
            mPasswordView.setText(DataStorage.getInstance().getPassword());
            if(!disableFlag) {
                mEmailView.setVisibility(View.GONE);
                mPasswordView.setVisibility(View.GONE);
                mCheckBoxRemeber.setVisibility(View.GONE);
                mEmailSignInButton.setVisibility(View.GONE);
                mShowPassword.setVisibility(View.GONE);
                attemptLogin();
            }
        }

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, LoginActivity.class));
    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            disableFlag = false;
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            disableFlag = false;
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            disableFlag = false;
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if(!disableFlag) {
                showProgress(true);
                userLoginTask(email, password);

                mEmailView.setEnabled(false);
                mPasswordView.setEnabled(false);
                mEmailSignInButton.setEnabled(false);
                mCheckBoxRemeber.setEnabled(false);
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        if (!email.isEmpty() && email.length() > 4)
            return true;
        else
            return false;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation rotation = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.rotate);
                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
                    mProgressView.startAnimation(rotation);
                    mProgressView.setVisibility(View.VISIBLE);
                } else {
                    mProgressView.clearAnimation();
                    mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private void userLoginTask(final String mEmail, final String mPassword) {


        try {
            DataCacheManager.getInstance().removeResponseData();
            String url = ServiceURLManager.getInstance().getLoginUrl();
            Login login = new Login();
            login.setUsername(mEmail);
            login.setPassword(mPassword);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("login: jsonObject", jsonObject);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {

//                            DataStorage.getInstance().setResponse(response.toString());

                            ObjectMapper mapper = new ObjectMapper();
                            ArrayList<HubInfoList> obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<List<HubInfoList>>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            showProgress(false);
                            if (obj != null && obj.size() > 0) {

                                ModelManager.getInstance().setAllHubInfoLists(obj);
                                DataCacheManager.getInstance().saveResponseData(response.toString());

//                                new BackgroundTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                DataStorage.getInstance().setPassword(mPassword);
                                DataStorage.getInstance().setUsername(mEmail);
                                DataStorage.getInstance().setRemeber(remeberMe);
//                                String string = DataStorage.getInstance().getResponse();
//                                Log.e("loginResponse",string);


                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                finish();
                            } else {

                                mEmailView.setVisibility(View.VISIBLE);
                                mPasswordView.setVisibility(View.VISIBLE);
                                mCheckBoxRemeber.setVisibility(View.VISIBLE);
                                mEmailSignInButton.setVisibility(View.VISIBLE);
                                mShowPassword.setVisibility(View.VISIBLE);
                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.e("response", error);
                    showProgress(false);
                    mEmailView.setEnabled(true);
                    mPasswordView.setEnabled(true);
                    mEmailSignInButton.setEnabled(true);
                    mCheckBoxRemeber.setEnabled(true);

                    mEmailView.setVisibility(View.VISIBLE);
                    mPasswordView.setVisibility(View.VISIBLE);
                    mCheckBoxRemeber.setVisibility(View.VISIBLE);
                    mEmailSignInButton.setVisibility(View.VISIBLE);
                    mShowPassword.setVisibility(View.VISIBLE);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_incorrect_password), null);
//                    } else if(error != null && error.networkResponse != null ) {
//                        showError(""+error.networkResponse.statusCode, null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();


            mEmailView.setVisibility(View.VISIBLE);
            mPasswordView.setVisibility(View.VISIBLE);
            mCheckBoxRemeber.setVisibility(View.VISIBLE);
            mEmailSignInButton.setVisibility(View.VISIBLE);
            mShowPassword.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
//        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (mEmailView != null)
//            mEmailView.setVisibility(View.VISIBLE);
//        if (mPasswordView != null)
//            mPasswordView.setVisibility(View.VISIBLE);
//        if (mCheckBoxRemeber != null)
//            mCheckBoxRemeber.setVisibility(View.VISIBLE);
//        if (mEmailSignInButton != null)
//            mEmailSignInButton.setVisibility(View.VISIBLE);
//        if (mShowPassword != null)
//            mShowPassword.setVisibility(View.VISIBLE);
    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }

    private class BackgroundTask extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            ModelManager.getInstance().setAllCircleInfoList(ModelManager.getInstance().getAllHubInfoList());
            return null;
        }
    }
}

