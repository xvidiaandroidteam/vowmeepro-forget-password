package net.xvidia.vowmee.enterprise;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.Util.MyExceptionHandler;
import net.xvidia.vowmee.enterprise.network.ModelManager;
import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.network.model.ClusterVOList;
import net.xvidia.vowmee.enterprise.network.model.HubInfoList;
import net.xvidia.vowmee.enterprise.network.model.Login;
import net.xvidia.vowmee.enterprise.storage.sqlite.DataStorage;
import net.xvidia.vowmee.enterprise.storage.sqlite.manager.DataCacheManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

	private Toolbar toolbar;
	private RelativeLayout hubLayout, circleLayout, micoLayout, cameraLayout, crsLayout;
	private TextView hubTextView, circleTextView, micoTextView, storeTextView, crsTextView;
	private int intHub, intCircle, intMico, intCamera, intCrs;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
//		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				onBackPressed();
//
//			}
//		});
//		getSupportActionBar().setIcon(R.drawable.idea_logo);
		getSupportActionBar().setTitle("");
//		mActivityTitle = getTitle().toString();

		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setHomeButtonEnabled(false);

		hubTextView = (TextView) findViewById(R.id.count);
		circleTextView = (TextView) findViewById(R.id.count2);
		micoTextView = (TextView) findViewById(R.id.count3);
		storeTextView = (TextView) findViewById(R.id.count4);
		crsTextView = (TextView) findViewById(R.id.count5);

		hubLayout = (RelativeLayout) findViewById(R.id.hubLayout);
		hubLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				selectedLayout = intHub;
				openNextActivity(1);
			}
		});
		circleLayout = (RelativeLayout) findViewById(R.id.circleLayout);
		circleLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				selectedLayout = intCircle;
				openNextActivity(2);
			}
		});
		micoLayout = (RelativeLayout) findViewById(R.id.micoLayout);
		micoLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				selectedLayout = intMico;
				openNextActivity(3);
			}
		});
		cameraLayout = (RelativeLayout) findViewById(R.id.cameraLayout);
		cameraLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				selectedLayout = intCamera;
				openNextActivity(4);
			}
		});
		crsLayout = (RelativeLayout) findViewById(R.id.crsLayout);
		crsLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				selectedLayout = intCrs;
				openNextActivity(5);
			}
		});
//		Intent intent = getIntent();
//		if (null != intent) { //Null Checking
//			hubList = intent.getStringArrayListExtra("hubList");
//		}
	}
	private void setUpView(){
		intHub = ModelManager.getInstance().getAllHubInfoList().size();
		String stringHub = String.valueOf(intHub);
		intCircle = ModelManager.getInstance().getAllCircleInfoList().size();
		String stringCircle = String.valueOf(intCircle);
		intMico = ModelManager.getInstance().getAllMicoInfoList().size();
		String stringMico = String.valueOf(intMico);
		intCamera = ModelManager.getInstance().getAllCameraVoList().size();
		String stringStore = String.valueOf(intCamera);
		intCrs = ModelManager.getInstance().getAllCrsCameraVoList().size();
		String stringCrs = String.valueOf(intCrs);

		boolean showCamera = true;
		if(ModelManager.getInstance().getAllHubInfoList().size() <2) {
			hubLayout.setVisibility(View.GONE);

		}else{
			showCamera = false;
		}
		hubTextView.setText(stringHub);
		if(ModelManager.getInstance().getAllCircleInfoList().size() <2) {
			circleLayout.setVisibility(View.GONE);

		}else{

			showCamera = false;
		}
		circleTextView.setText(stringCircle);
		if(ModelManager.getInstance().getAllMicoInfoList().size() < 2){
			micoLayout.setVisibility(View.GONE);

		}else{
			showCamera = false;
		}

		micoTextView.setText(stringMico);
		if(ModelManager.getInstance().getAllCameraVoList().size() <1)
			cameraLayout.setVisibility(View.GONE);
		storeTextView.setText(stringStore);
		if(ModelManager.getInstance().getAllCrsCameraVoList().size() <1)
			crsLayout.setVisibility(View.GONE);
		crsTextView.setText(stringCrs);

		if(showCamera){
			ModelManager.getInstance().setClusterVoList((ArrayList) ModelManager.getInstance().getAllMicoInfoList().get(0).getClusterVOList());

			ArrayList<CameraVoList> cameraVoLists = new ArrayList<CameraVoList>();
			ArrayList<ClusterVOList> clusterVoLists = ModelManager.getInstance().getClusterVoList();

			for(ClusterVOList clusterVoListObj : clusterVoLists){
				ArrayList<CameraVoList> obj = (ArrayList) clusterVoListObj.getCameraVOList();

				for (CameraVoList cameraVOListObj : obj) {
//                                if(!clusterVoListObj.getClusterType().equalsIgnoreCase("CRS"))
					cameraVoLists.add(cameraVOListObj);
				}
			}
			ModelManager.getInstance().setCameraVoList(cameraVoLists);

			Intent intent = new Intent(this, ListActivity.class);
			Bundle bundle = new Bundle();
			bundle.putBoolean(AppConstant.DIRECT, false);
			bundle.putString(AppConstant.NEXTLIST, AppConstant.CAMERA);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			bundle.putBoolean(AppConstant.BACKPRESSED_CLOSED, true);
			intent.putExtras(bundle);
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			startActivity(intent);
			finish();
		}
	}

	private void openNextActivity(int position) {

		Intent intent = new Intent(MainActivity.this, ListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putBoolean(AppConstant.DIRECT, true);

		switch (position) {
			case 1:

				bundle.putString(AppConstant.NEXTLIST, AppConstant.HUB);


				break;
			case 2:

				bundle.putString(AppConstant.NEXTLIST, AppConstant.CIRCLE);


				break;
			case 3:
				bundle.putString(AppConstant.NEXTLIST, AppConstant.MICO);


				break;
			case 4:
				bundle.putString(AppConstant.NEXTLIST, AppConstant.CAMERA);

				break;
			case 5:

				bundle.putString(AppConstant.NEXTLIST, AppConstant.CRS_CAMERA);

				break;
		}

		intent.putExtras(bundle);
		startActivity(intent);


		/*DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int size = dm.widthPixels/3;

		CustomGrid adapter = new CustomGrid(MainActivity.this, web,imageId, colorId, size);
		grid=(GridView)findViewById(R.id.grid);
		grid.setAdapter(adapter);
		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				Intent intent = new Intent(MainActivity.this, ListActivity.class);
				Bundle bundle = new Bundle();
				bundle.putBoolean(AppConstant.DIRECT, true);
				switch (position) {
					case 0:

						bundle.putString(AppConstant.NEXTLIST, AppConstant.HUB);


						break;
					case 1:

						bundle.putString(AppConstant.NEXTLIST, AppConstant.CIRCLE);


						break;
					case 2:
						bundle.putString(AppConstant.NEXTLIST, AppConstant.MICO);


						break;
					case 3:
						bundle.putString(AppConstant.NEXTLIST, AppConstant.CLUSTER);

						break;
					case 4:

						bundle.putString(AppConstant.NEXTLIST, AppConstant.CAMERA);

						break;
					case 5:

						bundle.putString(AppConstant.NEXTLIST, AppConstant.CRS_CAMERA);

						break;
				}

				intent.putExtras(bundle);
				startActivity(intent);

			}
		});
*/
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, Login.class));
		// Sync the toggle state after onRestoreInstanceState has occurred.
//		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
//		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_logout) {
		
			new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Logout")
					.setMessage("Are you sure you want to logout?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
//							getSharedPreferences(DataStorage.MY_PREFERENCES,MODE_PRIVATE).edit().clear().commit();

							DataStorage.getInstance().setPassword("");
							DataStorage.getInstance().setUsername("");
							DataStorage.getInstance().setRemeber(false);
							DataStorage.getInstance().removeDataStorage();
							Intent intent = new Intent(MainActivity.this, LoginActivity.class);
							startActivity(intent);
							finish();
						}

					})
					.setNegativeButton("No", null)
					.show();

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void restoreData() {
		String response = DataCacheManager.getInstance().getResponseData();
		if(!response.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			ArrayList<HubInfoList> obj = null;
			try {
				obj = mapper.readValue(response.toString(), new TypeReference<List<HubInfoList>>() {
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (obj != null && obj.size() > 0) {

				ModelManager.getInstance().setAllHubInfoLists(obj);

				new BackgroundTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}
		}
	}

	private class BackgroundTask extends AsyncTask<Void,Void,Void> {
		@Override
		protected Void doInBackground(Void... params) {
			ModelManager.getInstance().setAllCircleInfoList(ModelManager.getInstance().getAllHubInfoList());
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
//			setupGridView();
			setUpView();
			super.onPostExecute(aVoid);
		}
	}

	@Override
	protected void onResume() {
//		if(ModelManager.getInstance().getAllHubInfoList().size()>1){
			restoreData();
//		}
		super.onResume();
	}
}
