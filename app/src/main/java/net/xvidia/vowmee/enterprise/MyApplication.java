package net.xvidia.vowmee.enterprise;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

public class MyApplication extends MultiDexApplication {



    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
//    private static final String TWITTER_KEY = "YpbvFU7nX5dTEgem7OEcHndex";
//    private static final String TWITTER_SECRET = "rSEq0l8cu4dFOqESVnYOsDwHqeRRdSQS3T8NEJTlg7duwDrNUf";


    private static Context context;

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    public void attachBaseContext(Context base) {
        MultiDex.install(base);
        super.attachBaseContext(base);
    }
    public static Context getAppContext() {
        return context;
    }

}
