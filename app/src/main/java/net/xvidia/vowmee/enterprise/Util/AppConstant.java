package net.xvidia.vowmee.enterprise.Util;

/**
 * Created by vasu on 31/3/16.
 */
public class AppConstant {

    public final static String NEXTLIST = "nextList";
    public final static String DIRECT= "directList";
    public final static String HUB = "hub";
    public final static String MICO = "mico";
    public final static String CLUSTER = "store";
    public final static String CAMERA = "camera";
    public final static String CIRCLE = "circle";
    public final static String CRS_CAMERA = "crs_camera";
    public final static String BACKPRESSED_CLOSED = "CLOSE";

}
