package net.xvidia.vowmee.enterprise;

/**
 * Created by vasu on 29/3/16.
 */

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Base64;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.Util.MyExceptionHandler;
import net.xvidia.vowmee.enterprise.listadapter.CameraListAdapter;
import net.xvidia.vowmee.enterprise.network.ServiceURLManager;
import net.xvidia.vowmee.enterprise.network.VolleySingleton;
import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.network.model.Login;
import net.xvidia.vowmee.enterprise.storage.sqlite.DataStorage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import me.tittojose.www.timerangepicker_library.TimeRangePickerDialog;
import tcking.github.com.giraffeplayer.GiraffePlayer;
import tv.danmaku.ijk.media.player.IMediaPlayer;

public class VideoViewNew extends AppCompatActivity implements TimeRangePickerDialog.OnTimeRangeSelectedListener {

    public static final String TIMERANGEPICKER_TAG = "timerangepicker";
    static int convertCount;
    static int countCurrent;
    /**
     * TODO: Set the path variable to a streaming video URL or a local media file
     * path.
     */
    private static ArrayList<String> cameraFileList;
    private static String liveUrl;
    private static boolean isLiveVideo;
    private static CameraVoList cameraObj;
    //    VideoView mVideoView, mVideoView1;
    GiraffePlayer giraffePlayer;
    String path;
    String path1;
    Uri uriVideo, uriVideo1;
    Context mContex;
    boolean isLive;
    ProgressBar progressBar = null;
    String url;
    TimeRangePickerDialog timePickerDialog;
    private TextView recordedButton;
    private TextView previousButton;
    private TextView nextButton;
    private TextView liveTag;
    private TextView camerNameTextView;
    private TextView clusterNameTextView;
    private String camerName;
    private String clusterName;
    private int videoOnePlaying = -1;
    private boolean firstTimeDialogView;
    private int mYear, mMonth, mDay;
    private String date;
    private DatePickerDialog dialog;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // TODO Auto-generated method stub
            Calendar calendar = Calendar.getInstance();
            int mToday = calendar.get(Calendar.DAY_OF_MONTH);
            if (view.isShown()) {
                mYear = year;
                mMonth = month + 1;
                mDay = day;
                if (mDay > mToday)
                    return;
                String monthZero = "0";
                String dayZero = "0";
                if (mMonth < 10)
                    monthZero = monthZero + mMonth;
                else
                    monthZero = "" + mMonth;

                if (mDay < 10)
                    dayZero = dayZero + mDay;
                else
                    dayZero = "" + mDay;
                date = "" + mYear + "-" + monthZero + "-" + dayZero;
                firstTimeDialogView = false;

                showTimePicker();
//                    trigger = false;
//                    return;
            }
        }
//                trigger = true;
//            }
    };
   /* private MediaPlayer.OnPreparedListener preparedListener = new MediaPlayer.OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {
            mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                               int arg2) {
                    // TODO Auto-generated method stub
                    hideProgressBar();
                    recordedButton.setVisibility(View.VISIBLE);
                    liveTag.setVisibility(View.VISIBLE);
                    liveTag.setText(getString(R.string.recorded));
                    liveTag.setBackgroundColor(Color.RED);
                    mVideoView.start();
                    videoOnePlaying = 1;
                    convertToMp4(convertCount + 1);
                }
            });

            hideProgressBar();
        }
    };
    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {

        public void onCompletion(MediaPlayer mp) {
            mp.stop();
            setup(false);

        }
    };

    private MediaPlayer.OnPreparedListener preparedListener1 = new MediaPlayer.OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {
            mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                               int arg2) {
                    hideProgressBar();
                    recordedButton.setVisibility(View.VISIBLE);
                    liveTag.setVisibility(View.VISIBLE);
                    liveTag.setText(getString(R.string.recorded));
                    liveTag.setBackgroundColor(Color.RED);
                    mVideoView1.start();
                    videoOnePlaying = 2;
                    convertToMp4(convertCount + 1);
                }
            });

            hideProgressBar();
        }
    };
    private MediaPlayer.OnCompletionListener completionListener1 = new MediaPlayer.OnCompletionListener() {

        public void onCompletion(MediaPlayer mp) {
            mp.stop();
            setup1(false);

        }
    };*/

    public static void setLiveUrl(String liveUrl) {
        VideoViewNew.liveUrl = liveUrl;
    }

    public static void setCameraFileList(ArrayList<String> cameraFileList) {
        VideoViewNew.cameraFileList = cameraFileList;
    }

    public static CameraVoList getCameraVoListObj() {
        return cameraObj;
    }

    public static void setCameraVoListObj(CameraVoList cameraObj) {
        VideoViewNew.cameraObj = cameraObj;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_video_view);
        countCurrent = 0;
        convertCount = 0;
        mContex = this;
        Vitamio.isInitialized(getApplicationContext());
//        mVideoView = (VideoView) findViewById(R.id.surface_view);
//        mVideoView1 = (VideoView) findViewById(R.id.surface_view1);
        giraffePlayer = new GiraffePlayer(this);
        giraffePlayer.onComplete(new Runnable() {
            @Override
            public void run() {
                //callback when video is finish
                if(isLiveVideo) {
                    final String finalMessage = getString(R.string.error_camera_not_accessible);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(VideoViewNew.this, R.style.AppCompatAlertDialogStyle)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage(finalMessage)
                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (giraffePlayer != null)
                                                giraffePlayer.stop();
                                            initializeLiveVideo1();
                                        }

                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((VideoViewActivity) mContex).finish();
                                        }

                                    })
                                    .show();
                        }
                    });
                }else{
                    try {
                        if (giraffePlayer != null) {
//                            if (giraffePlayer.isPlaying())
                                giraffePlayer.stop();

                            setup(false);
                        }
                    } catch (IllegalStateException e) {

                    } catch (Exception e) {

                    }
                }
            }
        }).onInfo(new GiraffePlayer.OnInfoListener() {
            @Override
            public void onInfo(int what, int extra) {
                switch (what) {
                    case IMediaPlayer.MEDIA_INFO_BUFFERING_START:
                        //do something when buffering start
                        break;
                    case IMediaPlayer.MEDIA_INFO_BUFFERING_END:
                        //do something when buffering end
                        break;
                    case IMediaPlayer.MEDIA_INFO_NETWORK_BANDWIDTH:
                        //download speed
//                        ((TextView) findViewById(R.id.tv_speed)).setText(Formatter.formatFileSize(getApplicationContext(),extra)+"/s");
                        break;
                    case IMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                        //do something when video rendering
//                        findViewById(R.id.tv_speed).setVisibility(View.GONE);
                        break;
                }
            }
        }).onError(new GiraffePlayer.OnErrorListener() {
            @Override
            public void onError(int what, int extra) {
                if(isLive) {
                    String errorMsg = "What: " + what + "Extra: " + extra;
                    if (what == 1 && extra == -5)
                        errorMsg = "Please check your internet connection";
                    else
                        errorMsg = "Unable to stream due to limited internet connectivity, please try again after sometime " + errorMsg;
                    final String finalMessage = errorMsg;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(VideoViewNew.this, R.style.AppCompatAlertDialogStyle)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage(finalMessage)
                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            initializeLiveVideo1();
                                        }

                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((VideoViewActivity) mContex).finish();
                                        }

                                    })
                                    .show();
                        }
                    });
                }else{
                    try {
                        if (giraffePlayer != null) {
//                            if (giraffePlayer.isPlaying())
                                giraffePlayer.stop();

                        }
                    } catch (IllegalStateException e) {

                    } catch (Exception e) {

                    }
                }
            }
        });
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
//        mVideoView1.setVisibility(View.GONE);
        recordedButton = (TextView) findViewById(R.id.recordedButton);
        previousButton = (TextView) findViewById(R.id.recordedPrevious);
        camerNameTextView = (TextView) findViewById(R.id.cameraName);
        clusterNameTextView = (TextView) findViewById(R.id.storename);
        nextButton = (TextView) findViewById(R.id.recordedNext);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (videoOnePlaying == 1) {
                    setup(true);
                } else if (videoOnePlaying == 2) {
                    setup1(true);
                }*/
                setup(true);
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (videoOnePlaying == 1) {
                    setup(false);
                } else if (videoOnePlaying == 2) {
                    setup1(false);
                }*/
                setup(false);
            }
        });
        recordedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLiveVideo) {
                    if (giraffePlayer != null) {
                        if (giraffePlayer.isPlaying()) {
                            giraffePlayer.pause();
                        }
                    }
                }
//                sendStopRequest(url);
                showDialog(999);
            }
        });
        timePickerDialog = TimeRangePickerDialog.newInstance(
                VideoViewNew.this, false);
        liveTag = (TextView) findViewById(R.id.liveTvLayout);
        liveTag.setVisibility(View.GONE);
        isLiveVideo = false;

        Intent intent = getIntent();
        if (null != intent) {
            isLive = intent.getBooleanExtra(AppConstant.DIRECT, true);
            camerName = intent.getStringExtra(AppConstant.CAMERA);
            clusterName = intent.getStringExtra(AppConstant.CLUSTER);
        }
        if (!isLive) {
            firstTimeDialogView = true;
            String dateStr = getTimeStampDDMMYYYY();
            String time = "10" + ":" + "00" + "/" + "19" + ":" + "50";

            String url = ServiceURLManager.getInstance().getCameraFileListUrl("" + cameraObj.getId(), dateStr, time);

            getPlayList(url, true);
//            showDialog(999);
        } else {
            firstTimeDialogView = false;
            initializeLiveVideo1();
        }

        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, MainActivity.class));

        long id = cameraObj.getId();
        String username = DataStorage.getInstance().getUsername();

        url = ServiceURLManager.getInstance().getStopVideoUrl(id, username);
        camerNameTextView.setText(camerName);
        clusterNameTextView.setText(clusterName);
        nextButton.setVisibility(View.GONE);
        previousButton.setVisibility(View.GONE);
    }

    public String getTimeStampDDMMYYYY() {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.ENGLISH);
        Date now = new Date();
//        now.getSeconds();
        String timeString = "" + formatter.format(now);
//        timeString = timeString+"-"+now.getSeconds();
        return timeString;
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 999) {
            Calendar calendar = Calendar.getInstance();
            int mYear = calendar.get(Calendar.YEAR);
            int mMonth = calendar.get(Calendar.MONTH);
            int mDay = calendar.get(Calendar.DAY_OF_MONTH);
            calendar.add(Calendar.DATE, 1);
            Date currentDate = calendar.getTime();

            dialog = new DatePickerDialog(this, R.style.AppCompatAlertDialogStyle, myDateListener, mYear, mMonth, mDay);
            dialog.getDatePicker().updateDate(mYear, mMonth, mDay);
            dialog.getDatePicker().setMaxDate(currentDate.getTime());

            calendar.add(Calendar.DATE, -14);
            Date minDate = calendar.getTime();
            dialog.getDatePicker().setMinDate(minDate.getTime());
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {

                    dialog.dismiss();
                    if (isLiveVideo) {
                        if (giraffePlayer != null)
                            if (!giraffePlayer.isPlaying())
                                giraffePlayer.start();
                    }
//            finish();
                }
            });
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialog.dismiss();
                    if (isLiveVideo) {
                       /* if (mVideoView != null) {
                            if (!mVideoView.isPlaying())
                                mVideoView.start();
                        }*/
                        if (giraffePlayer != null) {
                            if (!giraffePlayer.isPlaying())
                                giraffePlayer.start();
                        }
                    } else {
                        /*if (videoOnePlaying==1) {
                            if (mVideoView != null) {
                                if (!mVideoView.isPlaying())
                                    mVideoView.start();
                            }
                        } else if(videoOnePlaying==2) {
                            if (mVideoView1 != null) {
                                if (!mVideoView1.isPlaying()) {
                                    mVideoView1.start();
                                }
                            }
                        }*/
                        if (giraffePlayer != null) {
                            if (!giraffePlayer.isPlaying())
                                giraffePlayer.start();
                        } else {
                            if (firstTimeDialogView) {
                                finish();

                            }
                        }

                    }
                }
            });
            return dialog;
        }

        return null;
    }

    private void showTimePicker() {
        if (timePickerDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timePickerDialog.show(getSupportFragmentManager(), TIMERANGEPICKER_TAG);
               /* timePickerDialog.onDismiss(new DialogInterface() {
                    @Override
                    public void cancel() {
                        if (isLiveVideo) {
                            if (mVideoView != null) {
                                if (!mVideoView.isPlaying())
                                    mVideoView.start();
                            }
                        }else {
                            if (videoOnePlaying==1) {
                                if (mVideoView != null) {
                                    if (!mVideoView.isPlaying())
                                        mVideoView.start();
                                }
                            } else {
                                if(!firstTimeDialogView){
                                    if (mVideoView1 != null) {
                                        if (!mVideoView1.isPlaying()) {
                                            mVideoView1.start();
                                        }
                                    }
                                }else{
                                    finish();
                                }
                            }

                        }
                    }

                    @Override
                    public void dismiss() {
                        if (isLiveVideo) {
                            if (mVideoView != null) {
                                if (!mVideoView.isPlaying())
                                    mVideoView.start();
                            }
                        }else {
                            if (videoOnePlaying) {
                                if (mVideoView != null) {
                                    if (!mVideoView.isPlaying())
                                        mVideoView.start();
                                }
                            } else {
                                if(!firstTimeDialogView){
                                    if (mVideoView1 != null) {
                                        if (!mVideoView1.isPlaying()) {
                                            mVideoView1.start();
                                        }
                                    }
                                }else{
                                    finish();
                                }
                            }

                        }
                    }
                });*/

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        CameraListAdapter.wasrequestsent = false;
        stopMediaPlayer();

        /*if(dialog.getDatePicker().getVisibility() == View.VISIBLE) {

            dialog.dismiss();
            if(isLiveVideo){
                if(mVideoView!=null)
                    if(!mVideoView.isPlaying())
                        mVideoView.start();
            }
//            finish();
        }
        else
        if(timePickerDialog.getView().getVisibility() == View.VISIBLE)
            {
                timePickerDialog.dismiss();
                if(isLiveVideo){
                    if(mVideoView!=null)
                        if(!mVideoView.isPlaying())
                            mVideoView.start();
                }
//                finish();
            }
        else */
        if (isLiveVideo) {
            sendStopRequest(url);
            finish();
        } else {
            finish();
        }

    }

    private void stopMediaPlayer() {
        try {
            /*if (mVideoView != null) {
                if (mVideoView.isPlaying())
                    mVideoView.stopPlayback();
                mVideoView = null;
            }
            if (mVideoView1 != null) {
                if (mVideoView1.isPlaying())
                    mVideoView1.stopPlayback();
                mVideoView1 = null;
            }*/
            if (giraffePlayer != null) {
                if (giraffePlayer.isPlaying()) {
                    giraffePlayer.stop();
                }
                giraffePlayer = null;
            }
        } catch (Exception e) {

        }
    }

    private void sendStopRequest(String url) {
        try {
//            showProgress(true);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    finish();

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error != null && error.networkResponse == null) {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
                }

            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }

    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new AlertDialog.Builder(VideoViewNew.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.app_name)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });
    }

    /*
        private void initializeLiveVideo() {
            try {
                path = liveUrl;
                uriVideo = Uri.parse(path);
                mVideoView.setVideoURI(uriVideo);
                isLiveVideo = true;
                mVideoView.start();
                progressBar.setVisibility(View.VISIBLE);
                mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
    //                    mp.start();
                        mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                            @Override
                            public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                                           int arg2) {
                                // TODO Auto-generated method stub
                                progressBar.setVisibility(View.GONE);
                                mp.start();
                                liveTag.setVisibility(View.VISIBLE);
                                liveTag.setText(getString(R.string.live));
                                liveTag.setBackgroundColor(getResources().getColor(R.color.grid6));
                                recordedButton.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });
                mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
    //                    ((VideoViewActivity) mContex).finish();
                        final String finalMessage = getString(R.string.error_camera_not_accessible);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(VideoViewActivity.this,R.style.AppCompatAlertDialogStyle)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Error")
                                        .setMessage(finalMessage)
                                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if(mVideoView != null)
                                                    mVideoView.stopPlayback();
                                                initializeLiveVideo();
                                            }

                                        })
                                        .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                ((VideoViewActivity) mContex).finish();
                                            }

                                        })
                                        .show();
                            }
                        });

                    }
                });

                mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        String errorMsg = "What: " + what + "Extra: "+ extra;
                        if(what == 1 && extra == -5)
                            errorMsg = "Please check your internet connection";
                        else
                            errorMsg = "Unable to stream due to limited internet connectivity, please try again after sometime "+errorMsg;
                        final String finalMessage = errorMsg;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(VideoViewActivity.this,R.style.AppCompatAlertDialogStyle)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Error")
                                        .setMessage(finalMessage)
                                        .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                initializeLiveVideo();
                                            }

                                        })
                                        .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                ((VideoViewActivity) mContex).finish();
                                            }

                                        })
                                        .show();
                            }
                        });

                        return true;
                    }
                });
            } catch (IllegalStateException e) {

            } catch (Exception e) {

            }

        }*/
    private void initializeLiveVideo1() {
        try {
            path = liveUrl;
//            uriVideo = Uri.parse(path);
//            mVideoView.setVideoURI(uriVideo);
            isLiveVideo = true;
//            mVideoView.start();
            giraffePlayer.play(path);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            liveTag.setVisibility(View.VISIBLE);
            liveTag.setText(getString(R.string.live));
            liveTag.setBackgroundColor(getResources().getColor(R.color.grid6));
            recordedButton.setVisibility(View.VISIBLE);
            giraffePlayer.start();
           /* mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
//                    mp.start();
                    mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                                       int arg2) {
                            // TODO Auto-generated method stub
                            progressBar.setVisibility(View.GONE);
                            mp.start();
                            liveTag.setVisibility(View.VISIBLE);
                            liveTag.setText(getString(R.string.live));
                            liveTag.setBackgroundColor(getResources().getColor(R.color.grid6));
                            recordedButton.setVisibility(View.VISIBLE);
                        }
                    });
                }
            });
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                    ((VideoViewActivity) mContex).finish();
                    final String finalMessage = getString(R.string.error_camera_not_accessible);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(VideoViewActivity.this,R.style.AppCompatAlertDialogStyle)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage(finalMessage)
                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if(mVideoView != null)
                                                mVideoView.stopPlayback();
                                            initializeLiveVideo();
                                        }

                                    })
                                    .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((VideoViewActivity) mContex).finish();
                                        }

                                    })
                                    .show();
                        }
                    });

                }
            });

            mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    String errorMsg = "What: " + what + "Extra: "+ extra;
                    if(what == 1 && extra == -5)
                        errorMsg = "Please check your internet connection";
                    else
                        errorMsg = "Unable to stream due to limited internet connectivity, please try again after sometime "+errorMsg;
                    final String finalMessage = errorMsg;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(VideoViewActivity.this,R.style.AppCompatAlertDialogStyle)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage(finalMessage)
                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            initializeLiveVideo();
                                        }

                                    })
                                    .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            ((VideoViewActivity) mContex).finish();
                                        }

                                    })
                                    .show();
                        }
                    });

                    return true;
                }
            });*/
        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }

    }

    private void initializeVideo(boolean first) {
        try {
            if (cameraFileList != null && cameraFileList.size() > 0) {
                firstTimeDialogView = false;
                if (first && cameraFileList.size() > 2) {
                    countCurrent = cameraFileList.size() - 2;
                    nextButton.setVisibility(View.VISIBLE);
                    previousButton.setVisibility(View.VISIBLE);
                } else {
                    nextButton.setVisibility(View.VISIBLE);
                    previousButton.setVisibility(View.GONE);
                }
                convertCount = countCurrent;
//                countCurrent=count;
                path = convertToMp4(countCurrent);
                uriVideo = Uri.parse(path);
             /*   mVideoView.setVideoURI(uriVideo);
                MediaController mediaController = new MediaController(this);
                mediaController.setAnchorView(mVideoView);
                mVideoView.setMediaController(mediaController);
                mVideoView.setOnPreparedListener(preparedListener);
                mVideoView.start();*/
                giraffePlayer.play(path);
                recordedButton.setVisibility(View.VISIBLE);
                liveTag.setVisibility(View.VISIBLE);
                liveTag.setText(getString(R.string.recorded));
                liveTag.setBackgroundColor(Color.RED);
                giraffePlayer.start();
                /*Videoview 2 initialisation*/

             /*   uriVideo1 = Uri.parse(path1);
                mVideoView1.setVideoURI(uriVideo1);
//                MediaController mediaController1 = new MediaController(this);
//                mediaController1.setAnchorView(mVideoView1);
//                mVideoView1.setMediaController(mediaController1);
                mVideoView.setOnCompletionListener(completionListener);
                mVideoView1.setOnCompletionListener(completionListener1);
                mVideoView1.setOnPreparedListener(preparedListener1);
                mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        try {
                            if (mp != null) {
                                if (mp.isPlaying()) {
                                    mp.stop();
                                }
                            }
                        } catch (IllegalStateException e) {

                        } catch (Exception e) {

                        }
                        setup(false);
                        return true;
                    }
                });
                mVideoView1.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {

                        try {
                            if (mp != null) {
                                if (mp.isPlaying()) {
                                    mp.stop();
                                }
                            }
                        } catch (IllegalStateException e) {

                        } catch (Exception e) {

                        }
                        setup1(false);
                        return true;
                    }
                });*/
            }
        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }
    }

    private int getNextCount() {
        try {
            if (convertCount >= cameraFileList.size()) {
                convertCount = 0;
            } else {
                convertCount = convertCount + 1;
            }

//            Log.i("index count ", "" + convertCount);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
        return convertCount;
    }

    private int getNextVideoCount() {
        try {
            if (countCurrent >= cameraFileList.size()) {
                countCurrent = 0;
            } else {
                countCurrent = countCurrent + 1;
            }
            if (countCurrent <= (cameraFileList.size() - 1)) {
                nextButton.setVisibility(View.VISIBLE);
                previousButton.setVisibility(View.VISIBLE);
            } else if (countCurrent == (cameraFileList.size() - 1)) {
                nextButton.setVisibility(View.GONE);
                previousButton.setVisibility(View.VISIBLE);
            }
//            Log.i("index count ", "" + countCurrent);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
        return countCurrent;
    }

    private boolean checkValidCount(int count) {
        boolean valid = false;
        if (count < cameraFileList.size()) {
            valid = true;
        }
        return valid;
    }

    private int getPreviousCount() {
        try {
            countCurrent = countCurrent - 1;
            if (countCurrent < 0)
                countCurrent = 0;
            if (countCurrent == 0) {
                previousButton.setVisibility(View.GONE);
                nextButton.setVisibility(View.VISIBLE);
            }

//            Log.i("index count ", "" + countCurrent);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
        return countCurrent;
    }

   /* public void setup1(boolean previous) {
        try {
            showProgressBar();
            mVideoView1.setVisibility(View.GONE);
            mVideoView.setVisibility(View.VISIBLE);
            if (previous) {
                path = convertToMp4(getPreviousCount());
            } else {
                path = convertToMp4(getNextVideoCount());
            }

            if (path != null && !path.isEmpty()) {
                uriVideo = Uri.parse(path);
                if (uriVideo != null)
                    mVideoView.setVideoURI(uriVideo);
            }
            mVideoView.setOnPreparedListener(preparedListener);
        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }
    }*/

    public void setup(boolean previous) {
        try {
            showProgressBar();
//            mVideoView.setVisibility(View.GONE);
//            mVideoView1.setVisibility(View.VISIBLE);
            if (previous) {
                path1 = convertToMp4(getPreviousCount());
            } else {

                path1 = convertToMp4(getNextVideoCount());
            }
            if (path1 != null && !path1.isEmpty()) {
//                uriVideo1 = Uri.parse(path1);
//                if (uriVideo1 != null)
//                    mVideoView1.setVideoURI(uriVideo1);
                giraffePlayer.play(path1);
                giraffePlayer.start();
                hideProgressBar();
            }
//            mVideoView1.setOnPreparedListener(preparedListener1);
        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }
    }

    private String convertToMp4(int count) {
        String newPath = "";
        if (checkValidCount(count)) {
            convertCount = count;
            newPath = cameraFileList.get(count);
            if (!newPath.isEmpty()) {
                if (newPath.contains(".avi")) {
                    sendRequest(newPath, count);
                    newPath = newPath.replace(".avi", ".mp4");
                } else {
                    convertToMp4(count + 1);
                }
            }
        }
        return newPath;
    }

    private void sendRequest(final String filepath, final int count) {


        try {

            String url = ServiceURLManager.getInstance().getConvertToMp4Url(filepath);
            Login login = new Login();

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(login);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}",

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String newPath = filepath;
                                if (newPath.contains(".avi")) {
                                    newPath = newPath.replace(".avi", ".mp4");
                                }
                                cameraFileList.remove(count);
                                cameraFileList.add(count, newPath);
                            } catch (IllegalStateException e) {

                            } catch (Exception e) {

                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_OK) {
                        try {
                            String newPath = filepath;
                            if (newPath.contains(".avi")) {
                                newPath = newPath.replace(".avi", ".mp4");
                            }
                            cameraFileList.remove(count);
                            cameraFileList.add(count, newPath);
                        } catch (IllegalStateException e) {

                        } catch (Exception e) {

                        }
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private String getAuthHeader() {
        String authHeader = "";
        try {
            String auth = DataStorage.getInstance().getUsername() + ":" + DataStorage.getInstance().getPassword();
            byte[] data = auth.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            authHeader = "Basic " + new String(base64);

        } catch (UnsupportedEncodingException e) {

        }
//
        return authHeader;
    }

    private void showProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
           /* if (mVideoView != null) {
                if (mVideoView.isPlaying()) {
                    mVideoView.stopPlayback();
                    if (isLiveVideo)
                        sendStopRequest(url);
                }
            }
            if (mVideoView1 != null) {
                if (mVideoView1.isPlaying()) {
                    mVideoView1.stopPlayback();
                    if (isLiveVideo)
                        sendStopRequest(url);
                }
            }*/
            if (giraffePlayer != null) {
                if (giraffePlayer.isPlaying()) {
                    giraffePlayer.stop();
                    if (isLiveVideo)
                        sendStopRequest(url);
                }
                finish();
            }

        } catch (IllegalStateException e) {

        } catch (Exception e) {

        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (isLive) {
            try {
               /* if (mVideoView != null) {
                    if (mVideoView.isPlaying()) {
                        mVideoView.stopPlayback();
                    }
                }
                if (mVideoView1 != null) {
                    if (mVideoView1.isPlaying()) {
                        mVideoView1.stopPlayback();
                    }
                }
                finish();*/
            } catch (IllegalStateException e) {

            } catch (Exception e) {

            }
            initializeLiveVideo1();
        }
        if (giraffePlayer != null) {
            giraffePlayer.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onTimeRangeSelected(int startHour, int startMin, int endHour, int endMin) {

        String strHr = "" + startHour;
        String strMin = "" + startMin;
        String endHr = "" + endHour;
        String endMn = "" + endMin;
        if (strHr.length() == 1)
            strHr = "0" + strHr;
        if (strMin.length() == 1)
            strMin = "0" + strMin;
        if (endHr.length() == 1)
            endHr = "0" + endHr;
        if (endMn.length() == 1)
            endMn = "0" + endMn;

        String time = strHr + ":" + strMin + "/" + endHr + ":" + endMn;
//        String url = "http://192.168.10.111:9090/getPlayBackFilelist/"+cameraObj.getCamera().getId()+"/"+date+"/"+time;

        String url = ServiceURLManager.getInstance().getCameraFileListUrl("" + cameraObj.getId(), date, time);

        getPlayList(url, false);

    }

    private void getPlayList(String url, final boolean first) {
        showProgressBar();
        try {

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    ObjectMapper mapper = new ObjectMapper();
                    List<String> obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<List<String>>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (obj != null) {
                        if (obj.size() > 0) {

                            VideoViewNew.setCameraFileList((ArrayList) obj);
                            if (cameraFileList == null) {
                                showError(getString(R.string.error_no_files), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        firstTimeDialogView = true;
                                        showDialog(999);
                                    }
                                });
                            } else if (cameraFileList.size() == 0) {
                                showError(getString(R.string.error_no_files), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        firstTimeDialogView = true;
                                        showDialog(999);
                                    }
                                });
                            } else {
                                initializeVideo(first);
                            }
//                            Intent intent = new Intent(VideoViewActivity .this, VideoViewActivity.class);
//                            Bundle bundle = new Bundle();
//                            bundle.putBoolean(AppConstant.DIRECT, false);
//                            intent.putExtras(bundle);
//                            startActivity(intent);

                        } else {

                            showError(getString(R.string.error_no_files), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    firstTimeDialogView = true;
                                    showDialog(999);
                                }
                            });

                        }
                    } else {
                        showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                firstTimeDialogView = true;
                                showDialog(999);
                            }
                        });

                    }
                    hideProgressBar();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressBar();
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            firstTimeDialogView = true;
                            showDialog(999);
                        }
                    });
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }
    }
}
