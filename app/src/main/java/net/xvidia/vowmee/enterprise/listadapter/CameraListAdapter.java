package net.xvidia.vowmee.enterprise.listadapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.xvidia.vowmee.enterprise.ListActivity;
import net.xvidia.vowmee.enterprise.MyApplication;
import net.xvidia.vowmee.enterprise.R;
import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.VideoViewLatest;
import net.xvidia.vowmee.enterprise.VideoViewLatest;
import net.xvidia.vowmee.enterprise.network.ServiceURLManager;
import net.xvidia.vowmee.enterprise.network.VolleySingleton;
import net.xvidia.vowmee.enterprise.network.model.CameraStatus;
import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.storage.sqlite.DataStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.vov.vitamio.Vitamio;
import io.vov.vitamio.utils.Log;

/**
 * Created by vasu on 31/3/16.
 */
public class CameraListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private ArrayList<CameraVoList> nameList;
    public int count;
    public static boolean wasrequestsent = false;


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView cameraName;
        private TextView cameraSubName;
        private ImageView onOffStatusImage;
        private RelativeLayout listItemView;
        private TextView onOffStatusText;
        private boolean cameraFeedNotAvailable;

        public ViewHolder(View v) {
            super(v);
            cameraName = (TextView) v.findViewById(R.id.name);
            cameraSubName = (TextView) v.findViewById(R.id.subname);
            onOffStatusImage = (ImageView) v.findViewById(R.id.onOffStatus);
            onOffStatusText = (TextView) v.findViewById(R.id.tvOnOff);
            listItemView = (RelativeLayout) v.findViewById(R.id.listView);

        }


    }

    public CameraListAdapter(Context context, final ArrayList<CameraVoList> nameList) {

        this.context = context;
        this.nameList = nameList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_camera_item, parent, false);
        v.setMinimumWidth(parent.getMeasuredWidth());
        ViewHolder viewHolder = new ViewHolder(v);
        Vitamio.isInitialized(context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        try {
//            CameraVoList cameraVoList1 = new CameraVoList();
            final CameraVoList cameraVoListObj = nameList.get(position);
            final ViewHolder holder = (ViewHolder) viewHolder;
            holder.cameraName.setText(cameraVoListObj.getClusterName());
            holder.cameraSubName.setText(cameraVoListObj.getCameraName());

            String key = Integer.toString(cameraVoListObj.getId());
            if(ListActivity.map!=null) {
                CameraVoList cameraVoList1 = ListActivity.map.get(key);
                if (cameraVoList1 != null) {

                    holder.onOffStatusImage.setVisibility(View.VISIBLE);
                    holder.onOffStatusText.setVisibility(View.VISIBLE);
                    if (cameraVoList1.getCameraStatus().equalsIgnoreCase("ON")) {
                        holder.onOffStatusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.on));
                        holder.onOffStatusText.setText("");
                        holder.cameraFeedNotAvailable = false;
                    } else if (cameraVoList1.getCameraStatus().equalsIgnoreCase("OFF")) {
                        holder.onOffStatusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.off));
                        if (cameraVoList1.getStrLiveUrl() != null) {
                            String lastOn = "";
                            if (cameraVoList1.getLastOnOffStatusTime() != null) {
                                if (!cameraVoList1.getLastOnOffStatusTime().equalsIgnoreCase("0")) {
                                    String lastOnOffStatus = cameraVoList1.getLastOnOffStatusTime();
                                    DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH);
                                    Date date = format.parse(lastOnOffStatus);
                                    long time = date.getTime();
                                    lastOn = getDateDiffString(time);
                                    holder.onOffStatusText.setText(lastOn);
                                    holder.cameraFeedNotAvailable = false;
                                    if (getDateDayDiffString(time) > 15) {
                                        holder.cameraFeedNotAvailable = true;
                                    }
                                } else {
                                    holder.onOffStatusText.setText(context.getString(R.string.never_on));
                                    holder.cameraFeedNotAvailable = true;
                                }
                            }
                        }
                    } else {
                    }
                } else {
//                ho
                    holder.onOffStatusImage.setImageDrawable(context.getResources().getDrawable(R.drawable.off));
                }
            }else{
                holder.onOffStatusImage.setVisibility(View.GONE);
                holder.onOffStatusText.setVisibility(View.GONE);
            }

            holder.listItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.cameraFeedNotAvailable){
                        if(!wasrequestsent) {
                            showError(context.getString(R.string.error_feed_not_available), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                finish();
                                }
                            });
                        }
                    }else {
//
                            ListActivity.mRecyclerView.setClickable(false);

                        if(!wasrequestsent) {
                            VideoViewLatest.setCameraVoListObj(cameraVoListObj);
                            long id = cameraVoListObj.getId();
                            String cameraStatusUrl = ServiceURLManager.getInstance().getCameraStatusUrl(id);

                            checkCameraCurrentStatus(cameraStatusUrl, cameraVoListObj);
                        }
                    }
//                        Intent intent = new Intent(context, DateAndTimePicker.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(intent);
                }
            });


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (nameList == null)
            return 0;
        else
            count = nameList.size();
        return count;
    }

    public String getTimeStampDDMMYYYY(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd-MM-yyyy hh:mm", Locale.ENGLISH);
        Date now = new Date(time);
//        now.getSeconds();
        String timeString = "" + formatter.format(now);
//        timeString = timeString+"-"+now.getSeconds();
        return timeString;
//        return timeString;

    }
    public long getDateDayDiffString(long dateOne) {
        String dayString = "";
        long timeOne = dateOne;
        long timeTwo = new Date().getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long delta = (timeTwo - timeOne);
        long totalDays = delta / oneDay;
        return totalDays;
    }
    public String getDateDiffString(long dateOne) {
        String dayString = "";
        long timeOne = dateOne;
        long timeTwo = new Date().getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long oneHour = 1000 * 60 * 60;
        long oneMinute = 1000 * 60;
        long delta = (timeTwo - timeOne);
        long totalDays = delta / oneDay;
        long year = totalDays / 365;
        long rest = totalDays % 365;
        long month = rest / 30;
        rest = rest % 30;
        long weeks = rest / 7;
        long days = rest % 7;
        long hours = delta / oneHour;
        long minutes = delta % oneHour;
        minutes = minutes / oneMinute;
        if (year > 0 && year < 2) {
            dayString = year + " year ";
        } else if (year > 1) {
            dayString = year + " years ";
        }
        if (dayString.isEmpty()) {
            if (month > 0 && month < 2) {
                dayString = dayString + month + " month ";
            } else if (month > 1) {
                dayString = dayString + month + " months ";
            }
        }
        if (dayString.isEmpty()) {
            if (weeks > 0 && weeks < 2) {
                dayString = dayString + weeks + " week ";
            } else if (weeks > 1) {
                dayString = dayString + weeks + " weeks ";
            }
        }
        if (dayString.isEmpty()) {
            if (days > 0 && days < 2) {
                dayString = dayString + days + " day ";
            } else if (days > 1) {
                dayString = dayString + days + " days ";
            }
        }
        if (dayString.isEmpty()) {
            if (hours > 0 && hours < 2) {
                dayString = hours + " hour ";
            } else if (hours > 1) {
                dayString = hours + " hours ";
            }

        }
        if (dayString.isEmpty()) {
            if (minutes > 0 && minutes < 2) {
                dayString = dayString + minutes + " minutes ";
            } else if (minutes > 1) {
                dayString = dayString + minutes + " minutes ";
            }
        }
        if (dayString.isEmpty()) {
            dayString = "few seconds ago";
        } else {
            dayString = "off since " + dayString;
        }
        return dayString;
    }

    private void checkCameraCurrentStatus(String url, final CameraVoList cameraVoList) {
        wasrequestsent = true;
        showProgress(true);
        try {

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        String message = response.getString("message");
                        int statusCode = response.getInt("statusCode");
                        wasrequestsent = false;
                        if (statusCode == 200) {
                            ListActivity.mRecyclerView.setClickable(true);
                            if (message.equalsIgnoreCase("OFF")) {
//                                Toast.makeText(context, context.getString(R.string.error_camera_off), Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(context, VideoViewLatest.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(AppConstant.DIRECT, false);
                                bundle.putString(AppConstant.CAMERA, cameraVoList.getCameraName());
                                bundle.putString(AppConstant.CLUSTER, cameraVoList.getClusterName());
                                intent.putExtras(bundle);
                                context.startActivity(intent);

                            } else if (message.equalsIgnoreCase("ON")) {
//                                wasrequestsent = true;
                                if (cameraVoList.getStrLiveUrl() != null)
                                    if (cameraVoList.getStrLiveUrl().isEmpty()) {
                                        wasrequestsent = false;
//                                        Toast.makeText(context, context.getString(R.string.error_select_date), Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(context, VideoViewLatest.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean(AppConstant.DIRECT, false);
                                        bundle.putString(AppConstant.CAMERA, cameraVoList.getCameraName());
                                        bundle.putString(AppConstant.CLUSTER, cameraVoList.getClusterName());
                                        intent.putExtras(bundle);
                                        context.startActivity(intent);
                                    } else {
                                        long id = cameraVoList.getId();
                                        String username = DataStorage.getInstance().getUsername();
                                        String liveUrl = ServiceURLManager.getInstance().getStartVideoUrl(id, username);
                                        sendStartVideoRequest(liveUrl, cameraVoList);
                                    }
                            }
                        }
                        showProgress(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    ListActivity.mRecyclerView.setClickable(true);
                    if (error != null && error.networkResponse == null) {
                        Log.e("error", error);
                        showError(context.getString(R.string.error_general), null);
                        wasrequestsent = false;
//                        finish();

                    }
                }

            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }

    }

    private void sendStartVideoRequest(String url, final CameraVoList cameraVoList) {

        try {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, "{}", new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    ObjectMapper mapper = new ObjectMapper();
                    CameraStatus obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), CameraStatus.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (obj != null) {
                        int statusCode = obj.getStatusCode();
                        String message = obj.getMessage();
                        if (statusCode == 200) {
                            showProgress(false);
                            VideoViewLatest.setLiveUrl(cameraVoList.getStrLiveUrl());
                            Intent intent = new Intent(context, VideoViewLatest.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(AppConstant.DIRECT, true);
                            bundle.putString(AppConstant.CAMERA, cameraVoList.getCameraName());
                            bundle.putString(AppConstant.CLUSTER, cameraVoList.getClusterName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        } else {
                            showError(message, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//                                    Toast.makeText(context, context.getString(R.string.error_select_date), Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(context, VideoViewLatest.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(AppConstant.DIRECT, false);
                                    bundle.putString(AppConstant.CAMERA, cameraVoList.getCameraName());
                                    bundle.putString(AppConstant.CLUSTER, cameraVoList.getClusterName());
                                    intent.putExtras(bundle);
                                    context.startActivity(intent);
                                }
                            });
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error != null && error.networkResponse == null) {
                        showError(context.getString(R.string.error_general), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
                            }
                        });
                    }
                }

            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("username", DataStorage.getInstance().getUsername());
//                    params.put("password",DataStorage.getInstance().getPassword());
                    params.put("Authorization", getAuthHeader());
                    return params;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//			}
        } catch (Exception e) {

        }

    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }

    private String getAuthHeader() {

        String authHeader = "";

        try {
            String auth = DataStorage.getInstance().getUsername() + ":" + DataStorage.getInstance().getPassword();
//            String auth = "username:username";
            byte[] data = auth.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            authHeader = "Basic " + new String(base64);

        } catch (UnsupportedEncodingException e) {

        }
//
        return authHeader;
    }

    public void showProgress(final boolean show) {

        ((ListActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate);
                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
                    ListActivity.mProgressView.startAnimation(rotation);
                    ListActivity.mProgressView.setVisibility(View.VISIBLE);
                } else {
                    ListActivity.mProgressView.clearAnimation();
                    ListActivity.mProgressView.setVisibility(View.GONE);
                }
            }
        });
    }

}

