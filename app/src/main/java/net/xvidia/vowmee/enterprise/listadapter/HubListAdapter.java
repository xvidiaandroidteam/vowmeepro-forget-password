package net.xvidia.vowmee.enterprise.listadapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import net.xvidia.vowmee.enterprise.ListActivity;
import net.xvidia.vowmee.enterprise.R;
import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.network.ModelManager;
import net.xvidia.vowmee.enterprise.network.model.HubInfoList;

/**
 * Created by vasu on 31/3/16.
 */
public class HubListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Context context;
    private ArrayList<HubInfoList> nameList;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView cameraName;
        private LinearLayout listItemView;
        private ImageView leftIcon;


        public ViewHolder(View v) {
            super(v);
            cameraName = (TextView) v.findViewById(R.id.name);
            listItemView = (LinearLayout) v.findViewById(R.id.listView);
            leftIcon = (ImageView) v.findViewById(R.id.lefticon);
        }


    }

    public HubListAdapter(Context context, final ArrayList<HubInfoList> nameList) {

        this.context = context;
        this.nameList = nameList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_listitem, parent, false);
        v.setMinimumWidth(parent.getMeasuredWidth());
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder,final int position) {

            try {

                final HubInfoList hubInfoList = nameList.get(position);


                final ViewHolder holder = (ViewHolder) viewHolder;
                holder.cameraName.setText(hubInfoList.getHubName());
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.small_cluster2_min));
                holder.listItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModelManager.getInstance().setCircleInfoList((ArrayList) hubInfoList.getCirlceVoList());
                        Intent intent = new Intent(context, ListActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(AppConstant.DIRECT, false);
                        bundle.putString(AppConstant.NEXTLIST, AppConstant.CIRCLE);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtras(bundle);
                        ((ListActivity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        context.startActivity(intent);
                    }
                });

            } catch (NullPointerException e) {

            } catch (Exception e) {


        }

    }

    @Override
    public int getItemCount() {
        if(nameList==null)
            return 0;
        else
        return nameList.size();
    }
}
