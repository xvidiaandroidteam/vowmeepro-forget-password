package net.xvidia.vowmee.enterprise.listadapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import net.xvidia.vowmee.enterprise.ListActivity;
import net.xvidia.vowmee.enterprise.R;
import net.xvidia.vowmee.enterprise.Util.AppConstant;
import net.xvidia.vowmee.enterprise.network.ModelManager;
import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.network.model.ClusterVOList;
import net.xvidia.vowmee.enterprise.network.model.MicoInfoList;

/**
 * Created by vasu on 31/3/16.
 */
public class MicoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    private ArrayList<MicoInfoList> nameList;
    private ArrayList<ClusterVOList> nameList1;
    public int count;

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView cameraName;
        private LinearLayout listItemView;
        private ImageView leftIcon;

        public ViewHolder(View v) {
            super(v);
            cameraName = (TextView) v.findViewById(R.id.name);
            listItemView = (LinearLayout) v.findViewById(R.id.listView);
            leftIcon = (ImageView) v.findViewById(R.id.lefticon);
        }


    }

    public MicoListAdapter(Context context, final ArrayList<MicoInfoList> nameList) {

        this.context = context;
        this.nameList = nameList;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_listitem, parent, false);
        v.setMinimumWidth(parent.getMeasuredWidth());
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

            try {

                final MicoInfoList hubInfoList = nameList.get(position);
//                final ClusterVOList hubInfoList1 = nameList1.get(position);

                final ViewHolder holder = (ViewHolder) viewHolder;
                holder.cameraName.setText(hubInfoList.getMicoName());
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.small_mico_min));
                holder.listItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModelManager.getInstance().setClusterVoList((ArrayList) hubInfoList.getClusterVOList());

                        ArrayList<CameraVoList> cameraVoLists = new ArrayList<CameraVoList>();
                        ArrayList<ClusterVOList> clusterVoLists = ModelManager.getInstance().getClusterVoList();

                        for(ClusterVOList clusterVoListObj : clusterVoLists){
                            ArrayList<CameraVoList> obj = (ArrayList) clusterVoListObj.getCameraVOList();

                            for (CameraVoList cameraVOListObj : obj) {
//                                if(!clusterVoListObj.getClusterType().equalsIgnoreCase("CRS"))
                                    cameraVoLists.add(cameraVOListObj);
                            }
                        }
                        ModelManager.getInstance().setCameraVoList(cameraVoLists);

                        Intent intent = new Intent(context, ListActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(AppConstant.DIRECT, false);
                        bundle.putString(AppConstant.NEXTLIST, AppConstant.CAMERA);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtras(bundle);
                        ((ListActivity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        context.startActivity(intent);
                    }
                });

            } catch (NullPointerException e) {

            } catch (Exception e) {


        }

    }

    @Override
    public int getItemCount() {
        if (nameList == null)
            return 0;
        else
            count = nameList.size();
        return count;
    }
}
