package net.xvidia.vowmee.enterprise.network;

import net.xvidia.vowmee.enterprise.network.model.CameraVoList;
import net.xvidia.vowmee.enterprise.network.model.CircleInfoList;
import net.xvidia.vowmee.enterprise.network.model.ClusterVOList;
import net.xvidia.vowmee.enterprise.network.model.HubInfoList;
import net.xvidia.vowmee.enterprise.network.model.MicoInfoList;

import java.util.ArrayList;

/**
 * Created by vasu on 31/3/16.
 */
public class ModelManager {
    private static ModelManager instance = null;
    public static ModelManager getInstance() {
        if (instance == null) {
            instance = new ModelManager();
        }
        return instance;
    }

    private ModelManager(){

    }


    ArrayList<CircleInfoList> circleInfoList = new ArrayList<>();
    ArrayList<MicoInfoList> micoInfoList = new ArrayList<>();
    ArrayList<ClusterVOList> clusterVoList = new ArrayList<>();
    ArrayList<CameraVoList> cameraVoList = new ArrayList<>();
    ArrayList<HubInfoList> hubInfoLists = new ArrayList<>();



    ArrayList<CircleInfoList> allCircleInfoList = new ArrayList<>();
    ArrayList<MicoInfoList> allMicoInfoList = new ArrayList<>();
    ArrayList<ClusterVOList> allClusterVoList = new ArrayList<>();
    ArrayList<CameraVoList> allCameraVoList = new ArrayList<>();
    ArrayList<CameraVoList> crsCameraVoList = new ArrayList<>();
    ArrayList<HubInfoList> allHubInfoLists = new ArrayList<>();


    public ArrayList<CircleInfoList> getCircleInfoList() {
        return circleInfoList;
    }

    public void setCircleInfoList(ArrayList<CircleInfoList> circleInfoList) {
        this.circleInfoList = circleInfoList;
    }

    public ArrayList<MicoInfoList> getMicoInfoList() {
        return micoInfoList;
    }

    public void setMicoInfoList(ArrayList<MicoInfoList> micoInfoList) {
        this.micoInfoList = micoInfoList;
    }

    public ArrayList<ClusterVOList> getClusterVoList() {
        return clusterVoList;
    }

    public void setClusterVoList(ArrayList<ClusterVOList> clusterVoList) {
        this.clusterVoList = clusterVoList;
    }

    public ArrayList<CameraVoList> getCameraVoList() {
        return cameraVoList;
    }

    public void setCameraVoList(ArrayList<CameraVoList> cameraVoList) {
        this.cameraVoList = cameraVoList;
    }

    public ArrayList<HubInfoList> getHubInfoLists() {
        return hubInfoLists;
    }

    public void setHubInfoLists(ArrayList<HubInfoList> hubInfoLists) {
        this.hubInfoLists = hubInfoLists;
    }

    public ArrayList<MicoInfoList> getAllMicoInfoList() {
        return allMicoInfoList;
    }

    public ArrayList<CameraVoList> getAllCameraVoList() {
        return allCameraVoList;
    }
    public ArrayList<CameraVoList> getAllCrsCameraVoList() {
        return crsCameraVoList;
    }

    public ArrayList<HubInfoList> getAllHubInfoList() {
        return allHubInfoLists;
    }

    public ArrayList<ClusterVOList> getAllClusterVoList() {
        return allClusterVoList;
    }

    public ArrayList<CircleInfoList> getAllCircleInfoList() {
        return allCircleInfoList;
    }


    public void setAllHubInfoLists(ArrayList<HubInfoList> hubInfoLists) {

        this.allHubInfoLists = hubInfoLists;

    }

    public void setAllCircleInfoList(ArrayList<HubInfoList> hubInfoList) {
        if( hubInfoList != null) {
            allCircleInfoList = new ArrayList<CircleInfoList>();

            for (HubInfoList hubInfoListobj : hubInfoList) {
                ArrayList<CircleInfoList> obj = (ArrayList) hubInfoListobj.getCirlceVoList();
                for (CircleInfoList circleInfoObj : obj) {
                    allCircleInfoList.add(circleInfoObj);
                }
            }
            setAllMicoInfoList(allCircleInfoList);
        }
    }
    public void setAllMicoInfoList(ArrayList<CircleInfoList> circleInfoList) {
        if( circleInfoList != null) {
            allMicoInfoList = new ArrayList<MicoInfoList>();

            for (CircleInfoList circleInfoListObj : circleInfoList) {
                ArrayList<MicoInfoList> obj = (ArrayList) circleInfoListObj.getMicoVoList();
                for (MicoInfoList micoInfoListObj : obj) {
                    allMicoInfoList.add(micoInfoListObj);
                }
            }
            setAllClusterInfoList(allMicoInfoList);
        }
    }

    public void setAllClusterInfoList(ArrayList<MicoInfoList> micoInfoList) {
        if( micoInfoList != null) {
            allClusterVoList = new ArrayList<ClusterVOList>();

            for (MicoInfoList micoInfoListObj : micoInfoList) {
                ArrayList<ClusterVOList> obj = (ArrayList) micoInfoListObj.getClusterVOList();
                for (ClusterVOList clusterVOListObj : obj) {
                    allClusterVoList.add(clusterVOListObj);
                }
            }
            setAllCameraInfoList(allClusterVoList);
            setAllCrsCameraInfoList(allClusterVoList);
        }
    }

    public void setAllCameraInfoList(ArrayList<ClusterVOList> clusterInfoList) {
        if( clusterInfoList != null) {
            allCameraVoList = new ArrayList<CameraVoList>();

            for (ClusterVOList clusterInfoListObj : clusterInfoList) {
                ArrayList<CameraVoList> obj = (ArrayList) clusterInfoListObj.getCameraVOList();
                for (CameraVoList cameraVOListObj : obj) {
                    if(clusterInfoListObj.getClusterType().equalsIgnoreCase("MYIDEA"))
                    allCameraVoList.add(cameraVOListObj);
                }
            }
        }
    }
    public void setAllCrsCameraInfoList(ArrayList<ClusterVOList> clusterInfoList) {
        if( clusterInfoList != null) {
            crsCameraVoList = new ArrayList<CameraVoList>();

            for (ClusterVOList clusterInfoListObj : clusterInfoList) {
                ArrayList<CameraVoList> obj = (ArrayList) clusterInfoListObj.getCameraVOList();
                for (CameraVoList cameraVOListObj : obj) {
                    if(clusterInfoListObj.getClusterType().equalsIgnoreCase("CRS"))
                    crsCameraVoList.add(cameraVOListObj);
                }
            }
        }
    }


}