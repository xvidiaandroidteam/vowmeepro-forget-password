package net.xvidia.vowmee.enterprise.network;


import net.xvidia.vowmee.enterprise.MyApplication;
import net.xvidia.vowmee.enterprise.R;

public class ServiceURLManager {

    private static ServiceURLManager instance = null;

    private ServiceURLManager() {

    }

    public static ServiceURLManager getInstance() {
        if (instance == null) {
            instance = new ServiceURLManager();
        }
        return instance;
    }

    final String base_URL = MyApplication.getAppContext().getResources().getString(R.string.base_url);
    final String base_URL_dev = MyApplication.getAppContext().getResources().getString(R.string.base_url1);

//    final String base_URL_Live= MyApplication.getAppContext().getResources().getString(R.string.base_url_live);

    public String getLoginUrl() {
        String url = base_URL + "logindo";
        return url;
    }


    public String getCameraFileListUrl(String id,String date, String time) {
        String url = base_URL + "getPlayBackFilelist/"+id + "/" +date + "/" +time;
        url = url.replace("+","%2B");
        return url;
    }

    public String getConvertToMp4Url(String mp4File) {
        String url = base_URL + "convertToMp4?fullpath="+mp4File;
        url = url.replace("+","%2B");
        return url;
    }

    public String getStopVideoUrl(long id, String username) {
        String url = base_URL + "stopLiveCamera?cameraId=" + id + "&username="+username;
        url = url.replace("+","%2B");
        return url;
    }

    public String getStartVideoUrl(long id, String username) {
        String url = base_URL + "startLiveCamera?cameraId=" + id + "&username="+username;
        url = url.replace("+","%2B");
        return url;
    }

    public String getCameraStatusUrl(long id) {
        String url = base_URL+"getCameraStatus?cameraId=" + id;
        url = url.replace("+","%2B");
        return url;
    }

    public String getAllCameraStatusUrl() {
        String url = base_URL+"getRolewiseCameraMap";
        url = url.replace("+","%2B");
        return url;
    }

    public String getPasswordResetUrl(String email) {
        String url = base_URL_dev+"forgotPasswordApp?email="+ email;
        url = url.replace("+","%2B");
        return url;
    }
}
