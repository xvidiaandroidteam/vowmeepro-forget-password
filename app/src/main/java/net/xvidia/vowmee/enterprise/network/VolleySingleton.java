package net.xvidia.vowmee.enterprise.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Ravi.
 */
public class VolleySingleton {
    private static VolleySingleton mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private VolleySingleton() {
    }

    public static synchronized VolleySingleton getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new VolleySingleton();
            mCtx = ctx;
        }
        return mInstance;
    }

    public void refresh(String key,boolean fullexpire) {
        if (mRequestQueue != null) {
            getRequestQueue().getCache().invalidate(key,fullexpire);
        }
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
