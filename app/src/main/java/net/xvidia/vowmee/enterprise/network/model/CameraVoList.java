package net.xvidia.vowmee.enterprise.network.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by on 22-Mar-16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CameraVoList {


    @JsonProperty("strLiveUrl")
    private String strLiveUrl;

    @JsonProperty("is24HourRunning")
    private boolean is24HourRunning;

    @JsonProperty("id")
    private int id;

    @JsonProperty("cameraId")
    private String cameraId;

    public String getLastOnOffStatusTime() {
        return lastOnOffStatusTime;
    }

    public void setLastOnOffStatusTime(String lastOnOffStatusTime) {
        this.lastOnOffStatusTime = lastOnOffStatusTime;
    }

    @JsonProperty("lastOnOffStatusTime")
    private String lastOnOffStatusTime;

    @JsonProperty("cameraName")
    private String cameraName;

    public String getCameraStatus() {
        return cameraStatus;
    }

    public void setCameraStatus(String cameraStatus) {
        this.cameraStatus = cameraStatus;
    }

    @JsonProperty("cameraStatus")
    private String cameraStatus;

    public String getLongClusterId() {
        return longClusterId;
    }

    public void setLongClusterId(String longClusterId) {
        this.longClusterId = longClusterId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @JsonProperty("longClusterId")
    private String longClusterId;

    @JsonProperty("clusterName")
    private String clusterName;

    @JsonProperty("clusterVOList")
    private ClusterVOList clusterVOList;

    public ClusterVOList getClusterVOList() {
        return clusterVOList;
    }

    public void setClusterVOList(ClusterVOList clusterVOList) {
        this.clusterVOList = clusterVOList;
    }

    public String getStrLiveUrl() {
        return strLiveUrl;
    }

    public void setStrLiveUrl(String strLiveUrl) {
        this.strLiveUrl = strLiveUrl;
    }

    public boolean is24HourRunning() {
        return is24HourRunning;
    }

    public void setIs24HourRunning(boolean is24HourRunning) {
        this.is24HourRunning = is24HourRunning;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

}
