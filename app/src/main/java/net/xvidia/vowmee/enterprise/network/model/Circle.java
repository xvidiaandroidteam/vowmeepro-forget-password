package net.xvidia.vowmee.enterprise.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Circle {

  //private variables
  @JsonProperty("circleId")
  private int circleId;

  @JsonProperty("circleName")
  private String circleName;

  @JsonProperty("zsm_name")
  private String zsm_name;

  @JsonProperty("zsm_email")
  private String zsm_email;

  @JsonProperty("zsm_mobile")
  private String zsm_mobile;

  @JsonProperty("hub")
  private Hub hub;

  public int getCircleId() {
    return circleId;
  }

  public void setCircleId(int circleId) {
    this.circleId = circleId;
  }

  public String getCircleName() {
    return circleName;
  }

  public void setCircleName(String circleName) {
    this.circleName = circleName;
  }

  public String getZsm_name() {
    return zsm_name;
  }

  public void setZsm_name(String zsm_name) {
    this.zsm_name = zsm_name;
  }

  public String getZsm_email() {
    return zsm_email;
  }

  public void setZsm_email(String zsm_email) {
    this.zsm_email = zsm_email;
  }

  public String getZsm_mobile() {
    return zsm_mobile;
  }

  public void setZsm_mobile(String zsm_mobile) {
    this.zsm_mobile = zsm_mobile;
  }

  public Hub getHub() {
    return hub;
  }

  public void setHub(Hub hub) {
    this.hub = hub;
  }
}
