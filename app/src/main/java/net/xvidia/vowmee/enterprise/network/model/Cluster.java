package net.xvidia.vowmee.enterprise.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cluster {

  //private variables
  @JsonProperty("id")
  private int id;
//
//  @JsonProperty("clusterId")
//  private String clusterId;

  @JsonProperty("clusterName")
  private String clusterName;

  @JsonProperty("mi_code")
  private String mi_code;

  @JsonProperty("cluster_mobile")
  private String cluster_mobile;

  @JsonProperty("cluster_email")
  private String cluster_email;


  //private variables
  @JsonProperty("clusterType")
  private String clusterType;

  @JsonProperty("city")
  private int city;

  @JsonProperty("state")
  private int state;

  @JsonProperty("address")
  private String address;

  @JsonProperty("pincode")
  private String pincode;

  @JsonProperty("mico")
  private Mico mico;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getClusterName() {
    return clusterName;
  }

  public void setClusterName(String clusterName) {
    this.clusterName = clusterName;
  }

  public String getMi_code() {
    return mi_code;
  }

  public void setMi_code(String mi_code) {
    this.mi_code = mi_code;
  }

  public String getCluster_mobile() {
    return cluster_mobile;
  }

  public void setCluster_mobile(String cluster_mobile) {
    this.cluster_mobile = cluster_mobile;
  }

  public String getCluster_email() {
    return cluster_email;
  }

  public void setCluster_email(String cluster_email) {
    this.cluster_email = cluster_email;
  }

  public String getClusterType() {
    return clusterType;
  }

  public void setClusterType(String clusterType) {
    this.clusterType = clusterType;
  }

  public int getCity() {
    return city;
  }

  public void setCity(int city) {
    this.city = city;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public Mico getMico() {
    return mico;
  }

  public void setMico(Mico mico) {
    this.mico = mico;
  }
}
