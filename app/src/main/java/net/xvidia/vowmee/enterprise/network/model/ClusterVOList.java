package net.xvidia.vowmee.enterprise.network.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by on 22-Mar-16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClusterVOList {



    @JsonProperty("id")
    private int id;

    @JsonProperty("clusterName")
    private String clusterName;

    @JsonProperty("mi_code")
    private String mi_code;

    @JsonProperty("cluster_mobile")
    private String cluster_mobile;

    @JsonProperty("cluster_email")
    private String cluster_email;

    @JsonProperty("clusterType")
    private String clusterType;

    @JsonProperty("micoInfoList")
    private MicoInfoList micoInfoList;

    @JsonProperty("cameraVOList")
    private List<CameraVoList> cameraVOList;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getMi_code() {
        return mi_code;
    }

    public void setMi_code(String mi_code) {
        this.mi_code = mi_code;
    }

    public String getCluster_mobile() {
        return cluster_mobile;
    }

    public void setCluster_mobile(String cluster_mobile) {
        this.cluster_mobile = cluster_mobile;
    }

    public String getCluster_email() {
        return cluster_email;
    }

    public void setCluster_email(String cluster_email) {
        this.cluster_email = cluster_email;
    }

    public String getClusterType() {
        return clusterType;
    }

    public void setClusterType(String clusterType) {
        this.clusterType = clusterType;
    }

    public MicoInfoList getMicoInfoList() {
        return micoInfoList;
    }

    public void setMicoInfoList(MicoInfoList micoInfoList) {
        this.micoInfoList = micoInfoList;
    }

    public List<CameraVoList> getCameraVOList() {
        return cameraVOList;
    }

    public void setCameraVOList(List<CameraVoList> cameraVOList) {
        this.cameraVOList = cameraVOList;
    }
}
