package net.xvidia.vowmee.enterprise.network.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by on 22-Mar-16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HubInfoList {

//
//    @JsonProperty("hub")
//    private Hub hub;
    @JsonProperty("hubId")
    private int hubId;
    @JsonProperty("hubName")
    private String hubName;
    @JsonProperty("fsh_name")
    private String fsh_name;
    @JsonProperty("fsh_email")
    private String fsh_email;
    @JsonProperty("fsh_mobile")
    private String fsh_mobile;

    @JsonProperty("cirlceVoList")
    private List<CircleInfoList> cirlceVoList;

    public int getHubId() {
        return hubId;
    }

    public void setHubId(int hubId) {
        this.hubId = hubId;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getFsh_name() {
        return fsh_name;
    }

    public void setFsh_name(String fsh_name) {
        this.fsh_name = fsh_name;
    }

    public String getFsh_email() {
        return fsh_email;
    }

    public void setFsh_email(String fsh_email) {
        this.fsh_email = fsh_email;
    }

    public String getFsh_mobile() {
        return fsh_mobile;
    }

    public void setFsh_mobile(String fsh_mobile) {
        this.fsh_mobile = fsh_mobile;
    }

    public List<CircleInfoList> getCirlceVoList() {
        return cirlceVoList;
    }

    public void setCirlceVoList(List<CircleInfoList> cirlceVoList) {
        this.cirlceVoList = cirlceVoList;
    }
}
