package net.xvidia.vowmee.enterprise.network.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vasu on 30/3/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Login {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Login() {
    }


}

