package net.xvidia.vowmee.enterprise.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Mico {

  //private variables
  @JsonProperty("micoId")
  private int micoId;

  @JsonProperty("micoName")
  private String micoName;

  @JsonProperty("mico_email")
  private String mico_email;

  @JsonProperty("mico_mobile")
  private String mico_mobile;

  @JsonProperty("circle")
  private Circle circle;

  public int getMicoId() {
    return micoId;
  }

  public void setMicoId(int micoId) {
    this.micoId = micoId;
  }

  public String getMicoName() {
    return micoName;
  }

  public void setMicoName(String micoName) {
    this.micoName = micoName;
  }

  public String getMico_email() {
    return mico_email;
  }

  public void setMico_email(String mico_email) {
    this.mico_email = mico_email;
  }

  public String getMico_mobile() {
    return mico_mobile;
  }

  public void setMico_mobile(String mico_mobile) {
    this.mico_mobile = mico_mobile;
  }

  public Circle getCircle() {
    return circle;
  }

  public void setCircle(Circle circle) {
    this.circle = circle;
  }
}
