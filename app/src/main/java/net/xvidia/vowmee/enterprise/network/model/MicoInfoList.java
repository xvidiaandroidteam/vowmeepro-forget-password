package net.xvidia.vowmee.enterprise.network.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by on 22-Mar-16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MicoInfoList {

//    @JsonProperty("mico")
//    private Mico mico;
    @JsonProperty("clusterVOList")
    private List<ClusterVOList> clusterVOList;

    public int getMicoId() {
        return micoId;
    }

    public void setMicoId(int micoId) {
        this.micoId = micoId;
    }

    public String getMicoName() {
        return micoName;
    }

    public void setMicoName(String micoName) {
        this.micoName = micoName;
    }

    public String getMico_email() {
        return mico_email;
    }

    public void setMico_email(String mico_email) {
        this.mico_email = mico_email;
    }

    public String getMico_mobile() {
        return mico_mobile;
    }

    public void setMico_mobile(String mico_mobile) {
        this.mico_mobile = mico_mobile;
    }

    public CircleInfoList getCircleInfoList() {
        return circleInfoList;
    }

    public void setCircleInfoList(CircleInfoList circleInfoList) {
        this.circleInfoList = circleInfoList;
    }

    @JsonProperty("micoId")
    private int micoId;

    @JsonProperty("micoName")
    private String micoName;

    @JsonProperty("mico_email")
    private String mico_email;

    @JsonProperty("mico_mobile")
    private String mico_mobile;

    @JsonProperty("circleInfoList")
    private CircleInfoList circleInfoList;

    public List<ClusterVOList> getClusterVOList() {
        return clusterVOList;
    }

    public void setClusterVOList(List<ClusterVOList> clusterVOList) {
        this.clusterVOList = clusterVOList;
    }
}
