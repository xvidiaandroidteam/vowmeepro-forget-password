package net.xvidia.vowmee.enterprise.storage.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CacheDataSource {
	private SQLiteDatabase database;
	private final MySqliteHelper dbHelper;
	private static CacheDataSource INSTANCE = null;// new CachDataSource();

	public static CacheDataSource getInstance() {
		if (INSTANCE == null)
			INSTANCE = new CacheDataSource();
		return INSTANCE;
	}

	private CacheDataSource() {
		dbHelper = MySqliteHelper.getInstance();
	}

	public void open(Context c) throws SQLException {
		try {
			if (database == null) {
				database = dbHelper.getWritableDatabase();
			} else if (!database.isOpen()) {
				database = dbHelper.getWritableDatabase();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() {
		// Log.d("CachedDataSource", "Closed");
		dbHelper.close();
		database.close();
	}


	/*
	 * Facebook DATA
	 */

	public void saveResponseData(String settingObj) {
		try {
			if(settingObj == null)
				return;
			ContentValues values = new ContentValues();
			values.put(MySqliteHelper.COLUMN_RESPONSE,
					MySqliteHelper.COLUMN_RESPONSE);
			values.put(MySqliteHelper.COLUMN_RESPONSE_VALUE,
					settingObj);
			if (getResponseData() != null) {
				database.delete(MySqliteHelper.TABLE_NAME_RESPONSE,
						MySqliteHelper.COLUMN_RESPONSE + "="
								+ MySqliteHelper.COLUMN_RESPONSE, null);
			}
			database.insert(MySqliteHelper.TABLE_NAME_RESPONSE, null, values);
		} catch (Exception e) {
		}
	}

	public void removeResponseData() {
		int log = database.delete(MySqliteHelper.TABLE_NAME_RESPONSE, null, null);
		Log.i("deleted",""+log);
	}

	public String getResponseData() {
		String data = "";
		Cursor cursor = null;
		try {
			cursor = database.query(MySqliteHelper.TABLE_NAME_RESPONSE, null,
					MySqliteHelper.COLUMN_RESPONSE + "=?",
					new String[]{MySqliteHelper.COLUMN_RESPONSE}, null, null, null);

			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				data = cursor.getString(cursor.getColumnIndex(MySqliteHelper.COLUMN_RESPONSE_VALUE));
			}
			if (cursor != null){
				cursor.close();
			}
		} catch (Exception e) {
			if (cursor != null)
				cursor.close();
		}
		return data;
	}


}
