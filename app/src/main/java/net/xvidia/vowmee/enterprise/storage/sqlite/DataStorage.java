package net.xvidia.vowmee.enterprise.storage.sqlite;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import net.xvidia.vowmee.enterprise.MyApplication;


public class DataStorage {
	public static String MY_PREFERENCES = "SETTINGPREFERENCE";
	public static SharedPreferences sharedpreferences;
	public static final String STR_REMEMBER = "REMEBERME";
	public static final String STR_RESPONSE = "RESPONSE";
	public static final String STR_USERNAME = "USERNAME";
	public static final String STR_PASSWORD = "PASSWORD";
	private static DataStorage instance = null;


	public static DataStorage getInstance() {
		if (instance == null) {
			instance = new DataStorage();
		}
		return instance;
	}

	private SharedPreferences getSharedPreference() {
		sharedpreferences = MyApplication.getAppContext().getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		return sharedpreferences;
	}

	public String getUsername() {
		try {
			return getSharedPreference().getString(STR_USERNAME, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setUsername(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_USERNAME, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getPassword() {
		try {
			return getSharedPreference().getString(STR_PASSWORD, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setPassword(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_PASSWORD, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public boolean getRemember() {
		try {
			return getSharedPreference().getBoolean(STR_REMEMBER, false);
		} catch (Exception e) {
		}
		return false;
	}

	public void setRemeber(boolean value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(STR_REMEMBER, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

/*	public String getResponse() {
		try {
			return getSharedPreference().getString(STR_RESPONSE, "");
		} catch (Exception e) {

		}
		return "";
	}

	public void setResponse(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_RESPONSE, value);
			editor.commit();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}*/
	public void removeDataStorage(){
		Editor editor = getSharedPreference().edit();
		editor.clear();
		editor.commit();
	}
}