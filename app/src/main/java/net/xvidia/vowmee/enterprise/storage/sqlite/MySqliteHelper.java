package net.xvidia.vowmee.enterprise.storage.sqlite;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.xvidia.vowmee.enterprise.MyApplication;

public class MySqliteHelper extends SQLiteOpenHelper {
	/*TABLES*/
	public static final String TABLE_NAME_RESPONSE = "RESPONSE";

	/*COLUMNS*/
	public static final String COLUMN_RESPONSE = "ResponseId";
	public static final String COLUMN_RESPONSE_VALUE = "ResponseValue";

	private static final String DATABASE_NAME = "vowmeepro.db";
	private static final int DATABASE_VERSION =1;

	private static final String TYPE_TEXT = " TEXT";
	private static final String TYPE_INTEGER = " INTEGER";
	private static final String COMMA_SEP = ",";
	/* Caching table create sql statement */
	final String CREATE_TABLE_NAME_RESPONSE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME_RESPONSE + "(" + COLUMN_RESPONSE
			+ TYPE_TEXT+" PRIMARY KEY," + COLUMN_RESPONSE_VALUE +  TYPE_TEXT +")";


	private static MySqliteHelper INSTANCE = null;// new
													// MySqliteHelper(MyApplication.getAppContext());

	public static MySqliteHelper getInstance() {
		if (INSTANCE == null)
			INSTANCE = new MySqliteHelper(MyApplication.getAppContext());
		return INSTANCE;
	}

	private MySqliteHelper(Context cntxt) {
		super(cntxt, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		try {
			database.execSQL(CREATE_TABLE_NAME_RESPONSE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
//		Log.w(MySqliteHelper.class.getName(),
//				"Upgrading database from version " + oldVersion + " to "
//						+ newVersion + ", which will destroy all old data");
		try{
			switch(oldVersion) {
			/*	case 1:
					database.execSQL("ALTER TABLE " + TABLE_NAME_SETTING + " ADD COLUMN " + COLUMN_SETTING_BACKGROUND_MUSIC + " TEXT");
					database.execSQL(TABLE_CREATE_BACKGROUND_MUSIC);
				case 2:
					database.execSQL(TABLE_CREATE_INTERVAL_TIMER_PROGRAM);
					database.execSQL(TABLE_CREATE_INTERVAL_TIMER_ROUND);
				case 3:
					database.execSQL(TABLE_CREATE_BLE_DEVICE);
				case 4:
					database.execSQL(TABLE_CREATE_HEART_RATE_DATA);
				case 5:
					database.execSQL("ALTER TABLE " + TABLE_CREATE_INTERVAL_TIMER_ROUND + " ADD COLUMN " + COLUMN_PROGRAM_MEDIA + " TEXT");
					database.execSQL("ALTER TABLE " + TABLE_CREATE_INTERVAL_TIMER_ROUND + " ADD COLUMN " + COLUMN_PROGRAM_INTERVAL_ROUND_NAME + " TEXT");
*/
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
//		onCreate(db);
	}
}
