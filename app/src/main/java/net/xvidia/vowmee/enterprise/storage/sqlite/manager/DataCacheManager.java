package net.xvidia.vowmee.enterprise.storage.sqlite.manager;

import net.xvidia.vowmee.enterprise.MyApplication;
import net.xvidia.vowmee.enterprise.storage.sqlite.CacheDataSource;

public class DataCacheManager {

	private static DataCacheManager Instance = null;// new DataCacheManager();
	CacheDataSource dataSource = null;// CachDataSource.getInstance();

	public static DataCacheManager getInstance() {
		if (Instance == null)
			Instance = new DataCacheManager();
		return Instance;
	}

	private DataCacheManager() {
		dataSource = CacheDataSource.getInstance();
	}

	public void open() {
		dataSource.open(MyApplication.getAppContext());
	}

	public void close() {
		dataSource.close();
	}



	 /*
	  Social Auth Facebook DATA
	 */

	/**
	 * Save Response  data
	 * 
	 * @param obj
	 * @return boolean
	 */

	public boolean saveResponseData(String obj) {
		boolean retVal = false;
		try {
			if (obj != null) {
				dataSource.open(MyApplication.getAppContext());
				dataSource.saveResponseData(obj);
				dataSource.close();
				retVal = true;
			} else {
				return false;
			}
		} catch (Exception e) {
			retVal = false;
		}
		return retVal;
	}

	public String getResponseData() {
		String retVal = "";
		try {
				dataSource.open(MyApplication.getAppContext());
			retVal=dataSource.getResponseData();
				dataSource.close();
		} catch (Exception e) {
		}
		return retVal;
	}

	/**
	 * Clears database for a setting
	 * 
	 */
	public final void removeResponseData() {
		dataSource.open(MyApplication.getAppContext());
		dataSource.removeResponseData();
		dataSource.close();
	}



}
